=== Flexible Product Fields (Product Add-ons) for WooCommerce ===
Contributors: wpdesk,dyszczo,grola,piotrpo,marcinkolanko,mateuszgbiorczyk,sebastianpisula,bartj
Donate link: https://www.wpdesk.net/products/flexible-product-fields-pro-woocommerce/
Tags: woocommerce product fields, woocommerce product addons, extra product options, product add-ons, product fields
Requires at least: 4.5
Tested up to: 5.9
Requires PHP: 7.0
Stable tag: trunk
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

The plugin extends the product options with extra fields. It allows customers to configure the product using texts, numbers, checkboxes, dropdowns and multi-dropdowns, radio buttons, pickers and others.

== Description ==

The Flexible Product Fields plugin extends WooCommerce product options with additional fields. It is a product configurator that allows users to set a product tailored to their needs. Creating additional WooCommerce product fields in the management panel is very simple and gives a lot of options when making changes.

[youtube https://www.youtube.com/watch?v=SzWgc1cn67U]

#### Some examples of using FPF fields

* Text fields for entering prints on T-shirts or engraving
* Text fields with number and email/URL validation
* Select and Radio fields with predefined text options - e.g. for selecting sizes or components
* Color swatches and a color picker for selecting the color of the product
* Checkbox with an additional service such as gift wrapping or insurance
* Other fields that enrich the product page with text (Headings and Paragraphs) or images
* and other...

#### Areas where FPF fields are shown

WooCommerce custom product fields are visible on the product page in the area above or below the Add to Cart button *(depending on the settings)*.

WooCommerce product addons will also appear on the Cart and Checkout page *(as a summary - the fields cannot be edited there)* and in the dashboard in the Order Details.

* [>>> Go to the FPF client area demo <<<](https://wpde.sk/fpf-demo-repo) - here you can test the product configured with the **Flexible Product Fields** plugin (no registration needed)
* [>>> Go to the FPF admin demo <<<](https://demo.wpdesk.org/?utm_source=wordpress-repository&utm_medium=demo-link&utm_campaign=demo-flexible-product-fields-pro-woocommerce) - here you can test the admin panel of all WP Desk plugins including **Flexible Product Fields**

### FREE

#### Main features of the FREE version
* Adding WooCommerce extra product options on the product page
* Showing labels and field values in cart summary and checkout page [(read more)](https://www.wpdesk.net/docs/flexible-product-fields-woocommerce-docs/#Product_page_and_checkout)
* Marking a field as required or not [(read more)](https://www.wpdesk.net/docs/flexible-product-fields-woocommerce-docs/#required)
* Editing options for each placeholder and tooltip
* Assigning CSS classes for field targeting and styling
* Saving product configuration [(read more)](https://www.wpdesk.net/docs/flexible-product-fields-woocommerce-docs/#Save_product_configuration)

#### Fields available in the FREE version
* [Text](https://www.wpdesk.net/docs/flexible-product-fields-woocommerce-docs/#Single_Line_Text)
* [Textarea](https://www.wpdesk.net/docs/flexible-product-fields-woocommerce-docs/#Paragraph_Text)
* [Number](https://www.wpdesk.net/docs/flexible-product-fields-woocommerce-docs/#Number)
* [E-mail](https://www.wpdesk.net/docs/flexible-product-fields-woocommerce-docs/#E-mail) (New feature)
* [URL](https://www.wpdesk.net/docs/flexible-product-fields-woocommerce-docs/#URL) (New feature)
* [Checkbox](https://www.wpdesk.net/docs/flexible-product-fields-woocommerce-docs/#Checkbox)
* [Multi-checkbox](https://www.wpdesk.net/docs/flexible-product-fields-woocommerce-docs/#Multi-checkbox) (New feature)
* [Select](https://www.wpdesk.net/docs/flexible-product-fields-woocommerce-docs/#Select)
* [Multi-select](https://www.wpdesk.net/docs/flexible-product-fields-woocommerce-docs/#Multi-select)
* [Radio](https://www.wpdesk.net/docs/flexible-product-fields-woocommerce-docs/#Radio_Button)
* [Radio with images](https://www.wpdesk.net/docs/flexible-product-fields-woocommerce-docs/#Radio_with_images)
* [Radio with colors](https://www.wpdesk.net/docs/flexible-product-fields-woocommerce-docs/#Radio_with_colors) (New feature)
* [Time](https://www.wpdesk.net/docs/flexible-product-fields-woocommerce-docs/#Time) (New feature)
* [Color](https://www.wpdesk.net/docs/flexible-product-fields-woocommerce-docs/#Color) (New feature)
* [Heading](https://www.wpdesk.net/docs/flexible-product-fields-woocommerce-docs/#Heading) (New feature)
* [Paragraph](https://www.wpdesk.net/docs/flexible-product-fields-woocommerce-docs/#Paragraph) (New feature)
* [Image](https://www.wpdesk.net/docs/flexible-product-fields-woocommerce-docs/#Image) (New feature)
* [HTML](https://www.wpdesk.net/docs/flexible-product-fields-woocommerce-docs/#HTML) (New feature)

#### Assigning of additional fields in the FREE version

It is possible to assign a group of custom fields to a specific product.

### PRO

#### Main features of the PRO version
* **Order Group**
  Arranging field groups in order [(read more)](https://www.wpdesk.net/docs/flexible-product-fields-woocommerce-docs/#order-group)
* **WooCommerce custom price fields**
  Assigning a fixed or percentage price (of on an initial price) to a field [(read more)](https://www.wpdesk.net/docs/flexible-product-fields-woocommerce-docs/#Price)
* **Conditional logic for fields**
  Setting rules for conditional logic based on the values of other preset FPF fields [(read more)](https://www.wpdesk.net/docs/flexible-product-fields-woocommerce-docs/#Conditional_logic_for_fields)
* **Quick Support**
  Providing fast and priority Helpdesk Support via email
* **Duplication**
  Duplicating field groups [(read more)](https://www.wpdesk.net/docs/flexible-product-fields-woocommerce-docs/#duplication)

#### Fields available in the PRO version
* [Date](https://www.wpdesk.net/docs/flexible-product-fields-woocommerce-docs/#Date_Picker) with advanced date exclusion options
* [File Upload](https://www.wpdesk.net/docs/flexible-product-fields-woocommerce-docs/#File_Upload) (New feature)

#### Assigning of additional fields in the PRO version:
* Categories
* Tags
* All products

[Upgrade to Flexible Product Fields PRO Now &rarr;](https://www.wpdesk.net/products/flexible-product-fields-pro-woocommerce/)

== Installation	 ==

You can install this plugin like any other WordPress plugin.

1. Download and unzip the latest release zip file.
2. Upload the entire plugin directory to your /wp-content/plugins/ directory.
3. Activate the plugin through the Plugins menu in WordPress Administration.

You can also use WordPress uploader to upload plugin zip file in menu Plugins -> Add New -> Upload Plugin. Then go directly to point 3.

== Frequently Asked Questions ==

= Where can I find documentation? =

All documentation is available on page [Docs: Flexible Product Fields - WooCommerce](https://www.wpdesk.net/docs/flexible-product-fields-woocommerce-docs/).

= How to get technical support? =

We provide support for the free version in the plugin [Support Forum](https://wordpress.org/support/plugin/flexible-product-fields/). We answer within 72 hours. Please upgrade to [PRO version](https://www.wpdesk.net/products/flexible-product-fields-pro-woocommerce/) to get priority email support.

= Does FPF work with WooCommerce product varations? =

A product can have variations and FPF fields set at the same time. All features of the free version are compatible with the variations.

= How to change the CSS of a specific field? =

Go to field settings and assign a CSS class in the *"CSS Class"* input. Then go to the *Appearance Customizer > Additional CSS* tab and enter the custom CSS code using the assigned class.

= How do I add a field to the Checkout page? =

Flexible Product Fields is a plugin that inserts new fields to the product page. Adding a field to the Checkout page requires another plugin named [Flexible Checkout Fields](https://wordpress.org/plugins/flexible-checkout-fields/). This plugin is a powerful tool when it comes to customizing the entire order.

= Does the plugin support currency switchers? =

The [PRO version](https://www.wpdesk.net/products/flexible-product-fields-pro-woocommerce/) supports the currency conversion mechanism and works with the most popular plugins of this type:
- Multi Currency for WooCommerce
- Currency Switcher for WooCommerce
- WooCommerce Currency Switcher

The list of compatible plugins is growing and we are open to new integrations.

= How to translate field names for multilingual website? =

Our product will work with most other plugins supporting WooCommerce. Among them will be language plugins that support string translation:
- WPML
- Polylang
- Loco Translate

= Is it possible to assign a price to the FPF field? =

Adding a fixed or percentage price is a killer feature when the product add-on affect its price. This option is available in the [PRO version](https://www.wpdesk.net/products/flexible-product-fields-pro-woocommerce/).

= How to add a conditional field? =

[Conditional logic](https://www.wpdesk.net/docs/flexible-product-fields-woocommerce-docs/#Conditional_logic_for_fields) is a [PRO version](https://www.wpdesk.net/products/flexible-product-fields-pro-woocommerce/) feature that can show one field if a specific value is selected in another field.

== Screenshots ==

1. List of available fields.
2. Product add-ons configurator example - frontend display.
3. Editor of fields.
4. Fields groups.
5. Editor of fields groups.
6. Preview of fields in Cart.
7. Preview of fields in Admin Dashboard.

== Changelog ==

= 2.1.3 - 2022-03-09 =
* Added support for WooCommerce 6.3

= 2.1.2 - 2022-02-10 =
* Added support for WooCommerce 6.2

= 2.1.1 - 2022-01-18 =
* Fixed displaying Image field for PHP 8
* Fixed handling of custom CSS classes for fields
* Fixed issue related to refreshing rewrite rules
* Fixed URL validation for values containing special characters
* Changed CSS class for Date field
* Changed performance of Conditional Logic settings
* Added support for WordPress 5.9
* Added support for WooCommerce 6.1

= 2.1.0 - 2021-12-21 =
* Fixed removing nonexistent values for multiselect field
* Added new field type: E-mail
* Added new field type: URL
* Added new field type: Multi-checkbox
* Added new field type: Radio with colors
* Added new field type: Time
* Added new field type: Color
* Added new field type: Heading
* Added new field type: Paragraph
* Added new field type: Image
* Added new field type: HTML

= 2.0.3 - 2021-11-29 =
* Fixed searching for items for select field in plugin settings
* Added support for WooCommerce 6.0

= 2.0.2 - 2021-11-16 =
* Fixed data migration from old plugin version for floating values
* Fixed saving prices for fields when adding to cart
* Added dynamic search of product list

= 2.0.1 - 2021-11-04 =
* Changed plugin deactivation modal

= 2.0.0 - 2021-10-14 =
* Major Update!
* Dropped support for Flexible Product Fields PRO version lower than 2.0
* Added new admin page to manage plugin settings
* Added validation for fields on settings page

= 1.7.5 - 2021-09-13 =
* Fixed display of price with decimal value

= 1.7.4 - 2021-09-08 =
* Fixed adding multiple products to the cart
* Changed plugin deactivation modal
* Added filter to modify price label of field

= 1.7.3 - 2021-07-15 =
* Added support for WordPress 5.8
* Added support for WooCommerce 5.5

= 1.7.2 - 2021-05-20 =
* Added support for WooCommerce 5.3

= 1.7.1 - 2021-04-22 =
* Fixed validation rules
* Added support for WooCommerce 5.2

= 1.7.0 - 2021-03-30 =
* Changed validation rules for field value

= 1.6.5 - 2021-03-11 =
* Added support for WordPress 5.7
* Added support for WooCommerce 5.1

= 1.6.4 - 2021-02-25 =
* Fixed character limit option for Latin Extended characters

= 1.6.3 - 2021-02-11 =
* Fixed values of Assign to and Fields columns on Fields Groups
* Added support for WooCommerce 5.0

= 1.6.2 - 2021-01-14 =
* Updated hook for integration with FPF plugin

= 1.6.1 - 2021-01-11 =
* Changed data for tracker

= 1.6.0 - 2020-12-16 =
* Fixed CSS Class option for Heading field
* Fixed image upload for Radio with images field
* Changed URLs for docs
* Added hook for integration with FPF plugin

= 1.5.0 - 2020-12-01 =
* Fixed saving Checkbox field with no value set
* Changed generation of args for fields on Product Page
* Added new field type: Multi-select
* Added new field type: Number
* Added new field type: Radio with images
* Added ability to display tooltip for field lab
* Added generation of predefined product configuration
* Added notice asking for plugin review
* Added placeholder option for Select field
* Added support for WooCommerce 4.8
* Added support for WordPress 5.6

= 1.4.1 - 2020-10-29 =
* Fixed saving Fields Group as draft

= 1.4.0 - 2020-10-15 =
* Changed required PHP version to 7.0
* Added support for WooCommerce Currency Switcher (WOOCS) plugin
* Added support for WooCommerce 4.6

= 1.3.7 - 2020-10-01 =
* Updated screenshots

= 1.3.6 - 2020-09-23 =
* Added notice of planned change of required PHP version to 7.0

= 1.3.5 - 2020-09-17 =
* Fixed saving products prices for session
* Added support for Multi Currency for WooCommerce plugin
* Added support for Currency Switcher for WooCommerce plugin

= 1.3.4 - 2020-09-10 =
* Fixed JS error on settings page

= 1.3.3 - 2020-09-10 =
* Fixed permissions level for REST API endpoints
* Added support for WooCommerce 4.5

= 1.3.2 - 2020-08-10 =
* Added support for WordPress 5.5

1.3.1 - 2020-07-28
* Removed unneeded REACT dequeue.

1.3.0 - 2020-06-17
* Moved React to webpack.

1.2.19 - 2020-06-10
* Fixed warning on empty products or categories
* Fatal error on rest_api_init action with third party plugins

1.2.18 - 2020-05-26
* Fixed field label requirement
* Fixed fields validation

1.2.17 - 2020-05-05
* Added support for WooCommerce 4.1

1.2.16 - 2020-04-17
* Fixed options with ending spaces in value

1.2.15 - 2020-04-10
* Fixed validation on fields with options

1.2.14 - 2020-03-23
* Added validation for fields with options

1.2.13 - 2020-03-09
* Added additional security hardenings

1.2.12 - 2020-02-25
* Fixed percentage option price on variable products

1.2.11 - 2020-01-30
* Fixed WPML multicurrency compatibility

1.2.10 - 2019-12-17
* Fixed WPML compatibility

1.2.9 - 2019-12-06
* Fixed field values with ampersand

1.2.8 - 2019-11-04
* Added support for WooCommerce 3.8 and Wordpress 5.3

1.2.7 - 2019-10-30
* Fixed fatal error in fields group settings

1.2.6 - 2019-10-29
* Fixed fatal error in plugin settings page
* Fixed Wordpress 5.3 compatibility

1.2.5 - 2019-10-10
* Added support for WPDesk prefixed libraries

1.2.4 - 2019-09-18
* Fixed option price display when price is negative number

1.2.3 - 2019-09-03
* Fixed price display and calculation for various tax settings in WooCommerce

1.2.2 - 2019-08-12
* Added support for WooCommerce 3.7

= 1.2.1 - 2019-07-11 =
* Fixed rare chance for fatal error when activating plugin

= 1.2.0 - 2019-07-12 =
* Added support for WPDesk libraries

= 1.1.20 - 2019-05-16 =
* Fixed actions order with PRO plugin older versions

= 1.1.19 - 2019-05-13 =
* Fixed CSS padding

= 1.1.17 - 2019-04-23 =
* Removed Wordpress build in react scripts on FPF settings page

= 1.1.16 - 2019-04-05 =
* Added support for WooCommerce 3.6

= 1.1.15 - 2019-01-24 =
* Fixed add to cart button on related products

= 1.1.14 - 2018-12-03 =
* Fixed quotes in fields values

= 1.1.13 - 2018-11-15 =
* Fixed display price value 0
* Fixed missing CSS class in heading field

= 1.1.12 - 2018-10-16 =
* Added support for WooCommerce 3.5
* Dropped support for WooCommerce 3.0 and older (the plugin may still work with older versions but we do not declare official support)

= 1.1.11 - 2018-08-20 =
* Fixed price for percentage discount for simple products

= 1.1.10 - 2018-08-08 =
* Fixed issue with prevent multiple fields display on subcategories
* Fixed issue with display fields more than once - woocommerce_before_add_to_cart_button are triggered by third party plugins, ie. WooCommerce Subscriptions

= 1.1.9 - 2018-08-02 =
* Fixed optional labels by removing them

= 1.1.8 - 2018-06-26 =
* Fixed error with conflict in tracker

= 1.1.7 - 2018-06-25 =
* Tweaked tracker data anonymization
* Fixed tracker notice

= 1.1.6 - 2018-05-23 =
* Added support for WooCommerce 3.4

= 1.1.5 - 2018-03-26 =
* Fixed percentage price type for variable products
* Fixed display of select field the product/category
* Deleted display fields for group products

= 1.1.4 - 2018-03-06 =
* Fixed some minor notices

= 1.1.3 - 2018-03-01 =
* Fixed displaying images added to field labels on WooCommerce 3.3
* Fixed problem with displaying fields when product doesn't have a price
* Fixed problems with plugin deactivation on multisite
* Fixed warnings from WP Desk Tracker

= 1.1.2 - 2018-01-31 =
* Fixed issue with category and product search when the store uses a plain permalink

= 1.1.1 - 2018-01-15 =
* Added support for WooCommerce 3.3
* Fixed default option for group assignment

= 1.1 - 2017-12-13 =
* Added new field types: checkbox, select and radio
* Added character limit option for text fields

= 1.0.6 - 2017-11-08 =
* Fixed adding price field in checkout when fields group is assigned to category and product is variation
* Fixed problems with plugin activation with WooCommerce 3.2.x

= 1.0.5 - 2017-10-10 =
* Added support for WooCommerce 3.2
* Fixed bug that prevented entering decimal numbers in Firefox and Edge
* Fixed Polish translation for "is required filed"

= 1.0.4 - 2017-05-30 =
* Integrated WP Desk Tracker class to help us understand how you use the plugin (you need to opt in to enable it)
* Added upgrade link to quick links

= 1.0.3 - 2017-05-18 =
* Load assets locally to prevent random changes in CDNs
* Fixed error: Cannot find module "create-react-class"

= 1.0.2 - 2017-04-27 =
* Fixed variations pricing

= 1.0.1 - 2017-03-23 =
* Minor fixes related to WooCommerce 3.0
* Fixed prices display with exluding taxes
* Fixed saving product groups with enter key

= 1.0 - 2017-02-28 =
* First release!

