<?php

namespace WPDesk\FPF\Pro\Settings;

use WPDesk\FPF\Pro\Field\Type\CheckboxType;
use WPDesk\FPF\Pro\Field\Type\RadioColorsType;
use WPDesk\FPF\Pro\Field\Type\RadioImagesType;
use WPDesk\FPF\Pro\Field\Type\RadioType;
use WPDesk\FPF\Pro\Field\Type\SelectType;
use WPDesk\FPF\Pro\Settings\Option\LogicRulesConditionOption;
use WPDesk\FPF\Pro\Settings\Option\LogicRulesFieldOption;
use WPDesk\FPF\Pro\Settings\Option\LogicRulesValueOption;

/**
 * Supports management for REST API routes.
 */
class Page {

	/**
	 * {@inheritdoc}
	 */
	public function hooks() {
		add_filter( 'flexible_product_fields/admin_logic_settings', [ $this, 'set_logic_settings' ] );
	}

	/**
	 * @internal
	 */
	public function set_logic_settings( array $settings ) {
		return [
			'endpoints'        => [
				'field'     => Routes::REST_API_ROUTE_LOGIC_FIELD,
				'condition' => Routes::REST_API_ROUTE_LOGIC_CONDITION,
				'value'     => Routes::REST_API_ROUTE_LOGIC_VALUE,
			],
			'option_names'     => array_merge(
				$settings['option_names'],
				[
					'logic_field'     => LogicRulesFieldOption::FIELD_NAME,
					'logic_condition' => LogicRulesConditionOption::FIELD_NAME,
					'logic_value'     => LogicRulesValueOption::FIELD_NAME,
				]
			),
			'field_conditions' => [
				CheckboxType::FIELD_TYPE    => $this->get_field_type_conditions( CheckboxType::FIELD_TYPE ),
				SelectType::FIELD_TYPE      => $this->get_field_type_conditions( SelectType::FIELD_TYPE ),
				RadioType::FIELD_TYPE       => $this->get_field_type_conditions( RadioType::FIELD_TYPE ),
				RadioImagesType::FIELD_TYPE => $this->get_field_type_conditions( RadioImagesType::FIELD_TYPE ),
				RadioColorsType::FIELD_TYPE => $this->get_field_type_conditions( RadioColorsType::FIELD_TYPE ),
			],
		];
	}

	private function get_field_type_conditions( string $field_type ): array {
		switch ( $field_type ) {
			case CheckboxType::FIELD_TYPE:
				return [
					[
						'value'       => 'is',
						'label'       => __( 'is', 'flexible-product-fields-pro' ),
						'has_options' => false,
						'values'      => [
							[
								'value' => 'checked',
								'label' => __( 'checked', 'flexible-product-fields-pro' ),
							],
							[
								'value' => 'unchecked',
								'label' => __( 'unchecked', 'flexible-product-fields-pro' ),
							],
						],
					],
					[
						'value'       => 'is_not',
						'label'       => __( 'is not', 'flexible-product-fields-pro' ),
						'has_options' => false,
						'values'      => [
							[
								'value' => 'checked',
								'label' => __( 'checked', 'flexible-product-fields-pro' ),
							],
							[
								'value' => 'unchecked',
								'label' => __( 'unchecked', 'flexible-product-fields-pro' ),
							],
						],
					],
				];
			case SelectType::FIELD_TYPE:
			case RadioType::FIELD_TYPE:
			case RadioImagesType::FIELD_TYPE:
			case RadioColorsType::FIELD_TYPE:
				return [
					[
						'value'       => 'is',
						'label'       => __( 'is', 'flexible-product-fields-pro' ),
						'has_options' => true,
						'values'      => [],
					],
					[
						'value'       => 'is_not',
						'label'       => __( 'is not', 'flexible-product-fields-pro' ),
						'has_options' => true,
						'values'      => [],
					],
				];
			default:
				return [];
		}
	}
}
