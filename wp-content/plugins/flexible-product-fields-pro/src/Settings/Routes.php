<?php

namespace WPDesk\FPF\Pro\Settings;

use WPDesk\FPF\Free\Settings\Route\RouteIntegration;
use WPDesk\FPF\Pro\Settings\Route\ProductsCatsRoute;
use WPDesk\FPF\Pro\Settings\Route\ProductsTagsRoute;

/**
 * Supports management for REST API routes.
 */
class Routes {

	const REST_API_ROUTE_LOGIC_FIELD     = 'fields-field';
	const REST_API_ROUTE_LOGIC_CONDITION = 'fields-condition';
	const REST_API_ROUTE_LOGIC_VALUE     = 'fields-value';

	/**
	 * Initializes actions for class.
	 *
	 * @return void
	 */
	public function init() {
		( new RouteIntegration( new ProductsCatsRoute() ) )->hooks();
		( new RouteIntegration( new ProductsTagsRoute() ) )->hooks();
	}
}
