/* eslint-disable no-var */
/* global swiffyslider */
/**
 * File kb-carousel-init.js.
 * Gets carousel working for post carousel blocks.
 */
( function() {
	'use strict';
	var kadenceBlocksProSwiffy = {
		cache: [],
		/**
		 * Initiate the script to process all
		 */
		initAll: function() {
			var swiffySliders = document.querySelectorAll( '.kadence-swiffy-slider-init' );
			for ( let i = 0; i < swiffySliders.length; i++ ) {
				const sliderSpeed = parseInt( swiffySliders[ i ].parentNode.getAttribute( 'data-slider-speed' ) ),
					sliderArrows = swiffySliders[ i ].parentNode.getAttribute( 'data-slider-arrows' ),
					sliderDots = swiffySliders[ i ].parentNode.getAttribute( 'data-slider-dots' ),
					sliderAuto = swiffySliders[ i ].parentNode.getAttribute( 'data-slider-auto' ),
					scroll = parseInt( swiffySliders[ i ].parentNode.getAttribute( 'data-slider-scroll' ) );
				swiffySliders[ i ].classList.add( 'swiffy-slider', 'slider-item-snapstart', 'slider-nav-visible' );
				if ( 1 !== scroll ) {
					swiffySliders[ i ].classList.add( 'slider-nav-page' );
				}
				if ( sliderAuto ) {
					swiffySliders[ i ].classList.add( 'slider-nav-autoplay' );
					swiffySliders[ i ].setAttribute( 'data-slider-nav-autoplay-interval', sliderSpeed );
				}
				swiffySliders[ i ].firstChild.classList.add( 'slider-container', 'slick-initialized' );
				if ( 'none' !== sliderArrows ) {
					const prevButton = document.createElement("BUTTON");
					prevButton.classList.add( 'slider-nav' );
					const nextButton = document.createElement("BUTTON");
					nextButton.classList.add( 'slider-nav', 'slider-nav-next' );
					swiffySliders[ i ].appendChild( prevButton );
					swiffySliders[ i ].appendChild( nextButton );
				}
				if ( 'none' !== sliderDots ) {
					swiffySliders[ i ].classList.add( 'slider-indicators-outside' );
					const indicators = document.createElement("UL");
					indicators.classList.add( 'slider-indicators' );
					for ( let n = 0; n < swiffySliders[ i ].firstChild.children.length; n++ ) {
						indicators.appendChild( document.createElement("LI") );
					}
					swiffySliders[ i ].appendChild( indicators );
				}
				kadenceBlocksProSwiffy.cache[ i ] = swiffyslider.initSlider( swiffySliders[ i ] );
			}
		},
		// Initiate the menus when the DOM loads.
		init: function() {
			if ( typeof swiffyslider !== undefined ) {
				kadenceBlocksProSwiffy.initAll();
			} else {
				// eslint-disable-next-line vars-on-top
				var initLoadDelay = setInterval( function() {
					if ( typeof swiffyslider !== undefined ) {
						kadenceBlocksProSwiffy.initAll();
						clearInterval( initLoadDelay );
					}
				}, 200 );
			}
		},
	};
	if ( 'loading' === document.readyState ) {
		// The DOM has not yet been loaded.
		document.addEventListener( 'DOMContentLoaded', kadenceBlocksProSwiffy.init );
	} else {
		// The DOM has already been loaded.
		kadenceBlocksProSwiffy.init();
	}
}() );
