<?php

namespace QuickCrossSellsVendor\WPDesk\Helper\Integration;

use QuickCrossSellsVendor\WPDesk\Helper\Page\SettingsPage;
use QuickCrossSellsVendor\WPDesk\PluginBuilder\Plugin\Hookable;
use QuickCrossSellsVendor\WPDesk\PluginBuilder\Plugin\HookableCollection;
use QuickCrossSellsVendor\WPDesk\PluginBuilder\Plugin\HookableParent;
/**
 * Integrates WP Desk main settings page with WordPress
 *
 * @package WPDesk\Helper
 */
class SettingsIntegration implements \QuickCrossSellsVendor\WPDesk\PluginBuilder\Plugin\Hookable, \QuickCrossSellsVendor\WPDesk\PluginBuilder\Plugin\HookableCollection
{
    use HookableParent;
    /** @var SettingsPage */
    private $settings_page;
    public function __construct(\QuickCrossSellsVendor\WPDesk\Helper\Page\SettingsPage $settingsPage)
    {
        $this->add_hookable($settingsPage);
    }
    /**
     * @return void
     */
    public function hooks()
    {
        $this->hooks_on_hookable_objects();
    }
}
