<?php

namespace QuickCrossSellsVendor\WPDesk\Logger\WC\Exception;

class WCLoggerAlreadyCaptured extends \RuntimeException
{
}
