<?php

namespace QuickCrossSellsVendor\WPDesk\Composer\Codeception\Commands;

use QuickCrossSellsVendor\Composer\Command\BaseCommand as CodeceptionBaseCommand;
use QuickCrossSellsVendor\Symfony\Component\Console\Output\OutputInterface;
/**
 * Base for commands - declares common methods.
 *
 * @package WPDesk\Composer\Codeception\Commands
 */
abstract class BaseCommand extends \QuickCrossSellsVendor\Composer\Command\BaseCommand
{
    /**
     * @param string $command
     * @param OutputInterface $output
     */
    protected function execAndOutput($command, \QuickCrossSellsVendor\Symfony\Component\Console\Output\OutputInterface $output)
    {
        \passthru($command);
    }
}
