<?php

namespace QuickCrossSellsVendor\WPDesk\Composer\Codeception;

use QuickCrossSellsVendor\WPDesk\Composer\Codeception\Commands\CreateCodeceptionTests;
use QuickCrossSellsVendor\WPDesk\Composer\Codeception\Commands\RunCodeceptionTests;
use QuickCrossSellsVendor\WPDesk\Composer\Codeception\Commands\RunLocalCodeceptionTests;
/**
 * Links plugin commands handlers to composer.
 */
class CommandProvider implements \QuickCrossSellsVendor\Composer\Plugin\Capability\CommandProvider
{
    public function getCommands()
    {
        return [new \QuickCrossSellsVendor\WPDesk\Composer\Codeception\Commands\CreateCodeceptionTests(), new \QuickCrossSellsVendor\WPDesk\Composer\Codeception\Commands\RunCodeceptionTests(), new \QuickCrossSellsVendor\WPDesk\Composer\Codeception\Commands\RunLocalCodeceptionTests()];
    }
}
