<?php

namespace QuickCrossSellsVendor\WPDesk\Composer\Codeception;

use QuickCrossSellsVendor\Composer\Composer;
use QuickCrossSellsVendor\Composer\IO\IOInterface;
use QuickCrossSellsVendor\Composer\Plugin\Capable;
use QuickCrossSellsVendor\Composer\Plugin\PluginInterface;
/**
 * Composer plugin.
 *
 * @package WPDesk\Composer\Codeception
 */
class Plugin implements \QuickCrossSellsVendor\Composer\Plugin\PluginInterface, \QuickCrossSellsVendor\Composer\Plugin\Capable
{
    /**
     * @var Composer
     */
    private $composer;
    /**
     * @var IOInterface
     */
    private $io;
    public function activate(\QuickCrossSellsVendor\Composer\Composer $composer, \QuickCrossSellsVendor\Composer\IO\IOInterface $io)
    {
        $this->composer = $composer;
        $this->io = $io;
    }
    /**
     * @inheritDoc
     */
    public function deactivate(\QuickCrossSellsVendor\Composer\Composer $composer, \QuickCrossSellsVendor\Composer\IO\IOInterface $io)
    {
        $this->composer = $composer;
        $this->io = $io;
    }
    /**
     * @inheritDoc
     */
    public function uninstall(\QuickCrossSellsVendor\Composer\Composer $composer, \QuickCrossSellsVendor\Composer\IO\IOInterface $io)
    {
        $this->composer = $composer;
        $this->io = $io;
    }
    public function getCapabilities()
    {
        return [\QuickCrossSellsVendor\Composer\Plugin\Capability\CommandProvider::class => \QuickCrossSellsVendor\WPDesk\Composer\Codeception\CommandProvider::class];
    }
}
