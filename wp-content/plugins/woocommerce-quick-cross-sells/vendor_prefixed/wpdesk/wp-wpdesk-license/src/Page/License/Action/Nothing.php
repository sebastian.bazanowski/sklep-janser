<?php

namespace QuickCrossSellsVendor\WPDesk\License\Page\License\Action;

use QuickCrossSellsVendor\WPDesk\License\Page\Action;
/**
 * Do nothing.
 *
 * @package WPDesk\License\Page\License\Action
 */
class Nothing implements \QuickCrossSellsVendor\WPDesk\License\Page\Action
{
    public function execute(array $plugin)
    {
        // NOOP
    }
}
