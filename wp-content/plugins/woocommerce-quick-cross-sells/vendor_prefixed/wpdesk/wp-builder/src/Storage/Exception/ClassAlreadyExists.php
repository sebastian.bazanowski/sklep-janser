<?php

namespace QuickCrossSellsVendor\WPDesk\PluginBuilder\Storage\Exception;

class ClassAlreadyExists extends \RuntimeException
{
}
