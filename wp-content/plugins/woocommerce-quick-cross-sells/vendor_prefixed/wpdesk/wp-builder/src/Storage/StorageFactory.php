<?php

namespace QuickCrossSellsVendor\WPDesk\PluginBuilder\Storage;

class StorageFactory
{
    /**
     * @return PluginStorage
     */
    public function create_storage()
    {
        return new \QuickCrossSellsVendor\WPDesk\PluginBuilder\Storage\WordpressFilterStorage();
    }
}
