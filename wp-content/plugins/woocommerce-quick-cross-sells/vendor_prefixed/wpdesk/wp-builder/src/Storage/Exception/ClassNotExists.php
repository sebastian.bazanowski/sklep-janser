<?php

namespace QuickCrossSellsVendor\WPDesk\PluginBuilder\Storage\Exception;

class ClassNotExists extends \RuntimeException
{
}
