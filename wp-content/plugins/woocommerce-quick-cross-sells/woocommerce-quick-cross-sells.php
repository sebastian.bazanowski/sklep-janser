<?php
/**
	Plugin Name: Quick Cross-Sells WooCommerce
	Plugin URI: https://www.wpdesk.net/products/quick-cross-sells-woocommerce/
	Description: The easiest to use plugin for cross-selling on the product page.
	Product: Quick Cross-Sells WooCommerce
	Version: 1.1.1
	Author: WP Desk
	Author URI: https://www.wpdesk.net/
	Text Domain: woocommerce-quick-cross-sells
	Domain Path: /lang/
	Requires at least: 5.2
	Tested up to: 5.9
	WC requires at least: 5.9
	WC tested up to: 6.3
	Requires PHP: 7.0

	@package \WPDesk\QuickCrossSells

	Copyright 2016 WP Desk Ltd.

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

/* THESE TWO VARIABLES CAN BE CHANGED AUTOMATICALLY */
$plugin_version           = '1.1.1';
$plugin_release_timestamp = '2021-11-19 09:38';

$plugin_name        = 'Quick Cross-Sells WooCommerce';
$plugin_class_name  = '\WPDesk\QuickCrossSells\Plugin';
$plugin_text_domain = 'woocommerce-quick-cross-sells';
$product_id         = 'Quick Cross-Sells WooCommerce';
$plugin_file        = __FILE__;
$plugin_dir         = dirname( __FILE__ );

$requirements = array(
	'php'     => '7.0',
	'wp'      => '4.5',
	'plugins' => array(
		array(
			'name'      => 'woocommerce/woocommerce.php',
			'nice_name' => 'WooCommerce',
			'version'   => '3.0',
		),
	),
);

require __DIR__ . '/vendor_prefixed/wpdesk/wp-plugin-flow/src/plugin-init-php52.php';
