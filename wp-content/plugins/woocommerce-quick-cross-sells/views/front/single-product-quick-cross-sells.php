<?php
/**
 * Single product Quick Cross-Sells seciton.
 *
 * @package WPDesk\QuickCrossSells
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>

<?php if ( $cross_sells_section_title ) : ?>
	<h3 class="wcqcs-cross-sells-section-title">
		<?php echo esc_html( $cross_sells_section_title ); ?>
	</h3>
<?php endif; ?>

<table cellspacing="0" class="shop_table wcqcs-cross-sells">
	<tbody>
		<?php
		foreach ( $quick_cross_sell_products as $quick_cross_sell_product ) :
			if ( ! $quick_cross_sell_product->is_purchasable() ) {
				continue;
			}

			$quick_cross_sell = wcqcs_get_quick_cross_sell_by_product_ids_pair( $quick_cross_sell_product->get_id(), $product->get_id() );

			if ( ! $quick_cross_sell ) {
				continue;
			}

			$quick_cross_sell_product_permalink = $quick_cross_sell_product->is_visible() ? $quick_cross_sell_product->get_permalink() : '';
			?>
			<tr class="wcqcs-cross-sell-item">

				<td class="wcqcs-cross-sell-item-cb">
					<label>
						<input type="checkbox" name="wcqcs-add-to-cart[]" id="wcqcs-add-to-cart-<?php echo esc_attr( $quick_cross_sell_product->get_id() ); ?>" value="<?php echo esc_attr( $quick_cross_sell_product->get_id() ); ?>">
						<span class="screen-reader-text">
						<?php
							/* translators: %s: product name */
							echo esc_html( sprintf( __( 'Add also &ldquo;%s&rdquo; to cart', 'woocommerce-quick-cross-sells' ), $quick_cross_sell_product->get_title() ) );
						?>
						</span>
					</label>
				</td>

				<?php if ( 'no' !== get_option( 'wcqcs_show_images' ) ) : ?>
					<td class="wcqcs-cross-sell-item-image">
						<?php
						$quick_cross_sell_product_image = $quick_cross_sell_product->get_image();

						if ( ! $quick_cross_sell_product_permalink ) :
							echo wp_kses_post( $quick_cross_sell_product_image );
						else :
							?>
							<a target="_blank" href="<?php echo esc_url( $quick_cross_sell_product_permalink ); ?>">
								<?php echo wp_kses_post( $quick_cross_sell_product_image ); ?>
							</a>
						<?php endif; ?>
					</td>
				<?php endif; ?>

				<td class="wcqcs-cross-sell-item-product">

					<?php if ( $quick_cross_sell->get_product_cta() ) : ?>
						<h4 class="wcqcs-cross-sell-item-cta">
							<?php echo esc_html( $quick_cross_sell->get_product_cta() ); ?>
						</h4>
					<?php endif; ?>

					<?php if ( ! $quick_cross_sell_product_permalink ) : ?>
						<span class="wcqcs-cross-sell-item-name">
					<?php else : ?>
						<a class="wcqcs-cross-sell-item-name" target="_blank" href="<?php echo esc_url( $quick_cross_sell_product_permalink ); ?>">
						<?php
					endif;

						echo esc_html( $quick_cross_sell_product->get_name() );

					if ( ! $quick_cross_sell_product_permalink ) :
						?>
						</span>
					<?php else : ?>

							<svg class="wcqcs-cross-sell-item-name-icon" xmlns="http://www.w3.org/2000/svg" version="1.1" width="20" height="20" viewBox="0 0 20 20" aria-hidden="true">
							<path d="M9 3h8v8l-2-1V6.92l-5.6 5.59-1.41-1.41L14.08 5H10zm3 12v-3l2-2v7H3V6h8L9 8H5v7h7z"/></svg>

						</a>
					<?php endif; ?>

					<?php
					$quick_cross_sell_product_attributes = wcqcs_get_formatted_product_variation_attributes( $quick_cross_sell_product );
					if ( $quick_cross_sell_product_attributes ) :
						?>
						<dl class="variation wcqcs-cross-sell-item-variation">
						<?php foreach ( $quick_cross_sell_product_attributes as $attribute ) : ?>
								<dt class="<?php echo sanitize_html_class( 'variation-' . $attribute['key'] ); ?>"><?php echo wp_kses_post( $attribute['key'] ); ?>:</dt>
								<dd class="<?php echo sanitize_html_class( 'variation-' . $attribute['key'] ); ?>"><?php echo wp_kses_post( wpautop( $attribute['value'] ) ); ?></dd>
							<?php endforeach; ?>
						</dl>
					<?php endif; ?>

					<?php
					$quick_cross_sell_price_html = $quick_cross_sell->get_discounted_price_html();
					if ( $quick_cross_sell_price_html ) :
						?>
						<p class="price wcqcs-cross-sell-item-price">
						<?php echo wp_kses_post( $quick_cross_sell_price_html ); ?>
						</p>
					<?php endif; ?>

					<?php if ( 'onbackorder' === $quick_cross_sell_product->get_stock_status() ) : ?>
						<?php echo wp_kses_post( wc_get_stock_html( $quick_cross_sell_product ) ); ?>
					<?php endif; ?>

				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
