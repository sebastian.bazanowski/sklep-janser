<?php
/**
 * Quick Cross-Sells products options.
 *
 * @package WPDesk\QuickCrossSells
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>

<div class="options_group show_if_simple show_if_variable hidden">

	<h3 class="wcqcs-product-options-title">
		<?php esc_html_e( 'Quick Cross-Sells', 'woocommerce-quick-cross-sells' ); ?>
	</h3>

	<?php
		woocommerce_wp_text_input(
			array(
				'id'          => '_wcqcs_section_title',
				'label'       => __( 'Section title', 'woocommerce-quick-cross-sells' ),
				'placeholder' => get_option( 'wcqcs_section_default_title', wcqcs_get_default_wcq_section_title_value() ),
				'desc_tip'    => true,
				'description' => __( 'This will be shown above Quick Cross-Sells products list.', 'woocommerce-quick-cross-sells' ),
			)
		);
		?>

	<div class="wcqcs-products-wrap">
		<ul class="wcqcs-products js-wcqcs-products"></ul>
		<div class="button button-primary wcqcs-add-product-row-button js-wcqcs-add-product-row-button">
			<?php echo esc_html( __( 'Add product', 'woocommerce-quick-cross-sells' ) ); ?>
		</div>
	</div>

</div>
