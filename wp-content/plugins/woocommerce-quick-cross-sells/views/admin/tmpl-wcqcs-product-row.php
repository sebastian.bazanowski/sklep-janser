<?php
/**
 * Quick Cross-Sells products template.
 *
 * @package WPDesk\QuickCrossSells
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>

<script type="text/template" id="tmpl-wcqcs-product-row">
	<li class="wcqcs-product-row js-wcqcs-product-row" data-product-key="{{ data.row_key }}">

		<div class="wcqcs-product-row-head js-wcqcs-product-row-head">

			<div class="wcqcs-product-row-head-title-wrap">

				<p class="form-field wcqcs_row_product_id_field">
					<label for="wcqcs_row_product_id-{{ data.row_key }}">
						<?php esc_html_e( 'Product', 'woocommerce-quick-cross-sells' ); ?>
					</label>
					<?php echo wp_kses_post( wc_help_tip( __( 'Select the product that you want to be promoted with the current product. Supported types: simple, variation, simple subscription, subscription variation', 'woocommerce-quick-cross-sells' ) ) ); ?>
					<select class="wc-product-search" style="width: 80%;" id="wcqcs_row_product_id-{{ data.row_key }}" name="_wcqcs_products[{{ data.row_key }}][product_id]" data-placeholder="<?php esc_attr_e( 'Search for a product&hellip;', 'woocommerce-quick-cross-sells' ); ?>" data-action="wcqcs_json_search_products" data-exclude="<?php echo intval( $post->ID ); ?>" data-allow_clear="false">
						<# if ( data.product_id ) { #>
							<option selected="selected" value="{{ data.product_id }}">{{ data.product_name }}</option>
						<# } #>
					</select>
				</p>

			</div>

			<div class="wcqcs-remove-product-row js-wcqcs-remove-product-row">
				<?php esc_html_e( 'Remove', 'woocommerce-quick-cross-sells' ); ?>
			</div>

		</div>

		<div class="wcqcs-product-row-body">

			<p class="form-field wcqcs_row_product_cta_field">
				<label for="wcqcs_row_product_cta-{{ data.row_key }}">
					<?php esc_html_e( 'Product\'s call-to-action', 'woocommerce-quick-cross-sells' ); ?>
				</label>
				<?php echo wp_kses_post( wc_help_tip( __( 'This text will be shown above the product title.', 'woocommerce-quick-cross-sells' ) ) ); ?>
				<input type="text" class="short" name="_wcqcs_products[{{ data.row_key }}][product_cta]" id="wcqcs_row_product_cta-{{ data.row_key }}" value="{{ data.product_cta }}">
			</p>

			<p class=" form-field wcqcs_row_product_discount_type_field">
				<label for="wcqcs_row_product_discount_type-{{ data.row_key }}">
					<?php esc_html_e( 'Discount type', 'woocommerce-quick-cross-sells' ); ?>
				</label>
				<select id="wcqcs_row_product_discount_type-{{ data.row_key }}" name="_wcqcs_products[{{ data.row_key }}][product_discount_type]" class="select short js-wcqcs-product-row-discount-type">
					<option <# if ( 'none' == data.product_discount_type ) { #> selected="selected" <# } #> value="none"><?php esc_html_e( 'None', 'woocommerce-quick-cross-sells' ); ?></option>
					<option <# if ( 'percent' == data.product_discount_type ) { #> selected="selected" <# } #> value="percent"><?php esc_html_e( 'Percentage discount', 'woocommerce-quick-cross-sells' ); ?></option>
					<option <# if ( 'fixed' == data.product_discount_type ) { #> selected="selected" <# } #> value="fixed"><?php esc_html_e( 'Fixed discount', 'woocommerce-quick-cross-sells' ); ?></option>
				</select>
			</p>

			<p class="form-field wcqcs_row_product_discount_field js-wcqcs-row-product-discount-field">
				<label for="wcqcs_row_product_discount-{{ data.row_key }}">
					<?php esc_html_e( 'Discount amount', 'woocommerce-quick-cross-sells' ); ?>
				</label>
				<?php
				if ( class_exists( 'WC_Subscriptions' ) ) {
					echo wp_kses_post( wc_help_tip( __( 'For subscriptions, the discount will be granted for the initial payment only.', 'woocommerce-quick-cross-sells' ) ) );
				}
				?>
				<input type="text" class="short js-wcqcs-product-row-discount" name="_wcqcs_products[{{ data.row_key }}][product_discount]" id="wcqcs_row_product_discount-{{ data.row_key }}" value="{{ data.product_discount }}">
			</p>

		</div>

		<# if ( data.is_new_row ) { #>
			<input type="hidden" name="_wcqcs_products[{{ data.row_key }}][is_new_row]" value="1">
		<# } #>

	</li>
</script>
