/* global wcqcsProductQuickCrossSells */

jQuery(document).ready(function($){

	var product_rows_wrap 	= $('.js-wcqcs-products'),
		product_template 	= wp.template( 'wcqcs-product-row' ),
		products 			= wcqcsProductQuickCrossSells.products;

	var wcqcsToggleDiscountFieldsVisibility = function($select_el){

		var $row_el = $select_el.parents('li.js-wcqcs-product-row');

		if ( 'none' === $select_el.val() ) {
			$row_el.find('.js-wcqcs-row-product-discount-field').hide();
			$row_el.find('input.js-wcqcs-product-row-discount').val('');
		} else {
			$row_el.find('.js-wcqcs-row-product-discount-field').show();
		}

	}

	var wcqcsReinitWcElements = function(){

		$( document.body )
			.trigger( 'wc-enhanced-select-init' )
			.trigger( 'init_tooltips' );

		$( document.body )
			.find( 'select.js-wcqcs-product-row-discount-type' )
			.each( function( index ) {
				wcqcsToggleDiscountFieldsVisibility( $(this) );
			});

	}

	if ( products.length ) {
		$.each( products, function( key, product_data ) {
			product_rows_wrap.append( product_template( product_data ) );
		} );
	}

	product_rows_wrap.sortable({
		axis: 'y',
		handle: '.js-wcqcs-product-row-head',
		create: wcqcsReinitWcElements,
	});

	$(document.body)
		.on( 'click', '.js-wcqcs-add-product-row-button', function(e){

			e.preventDefault();

			var row_key = new Date().getTime() + ( ( ( 1 + Math.random() ) * 0x10000 ) | 0 ).toString(8);

			var new_product_row_data = {
				row_key: row_key,
				product_id: '',
				product_name: '',
	            product_cta: '',
	            product_discount_type: 'none',
	            product_discount: '',
	            is_new_row: true,
			};

		    product_rows_wrap
		    	.append( product_template( new_product_row_data ) )
		    	.sortable( 'refresh' );

		    wcqcsReinitWcElements();

		})
		.on( 'click', '.js-wcqcs-remove-product-row', function(e){

			e.preventDefault();

			if ( window.confirm( wcqcsProductQuickCrossSells.strings.i18n_remove_product_row ) ) {
				$(this).parents('.js-wcqcs-product-row').remove();
			}

		})
		.on( 'change', 'select.js-wcqcs-product-row-discount-type', function(e){

			wcqcsToggleDiscountFieldsVisibility( $(this) );

		});

});
