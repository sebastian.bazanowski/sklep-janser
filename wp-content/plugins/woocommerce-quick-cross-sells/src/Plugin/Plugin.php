<?php
/**
 * Plugin main class.
 *
 * @package WPDesk\QuickCrossSells
 */

namespace WPDesk\QuickCrossSells;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use QuickCrossSellsVendor\WPDesk_Plugin_Info;
use QuickCrossSellsVendor\WPDesk\PluginBuilder\Plugin\AbstractPlugin;
use QuickCrossSellsVendor\WPDesk\PluginBuilder\Plugin\HookableCollection;
use QuickCrossSellsVendor\WPDesk\PluginBuilder\Plugin\HookableParent;
use QuickCrossSellsVendor\WPDesk\PluginBuilder\Plugin\Activateable;

/**
 * Main plugin class. The most important flow decisions are made here.
 *
 * @package WPDesk\QuickCrossSells
 */
class Plugin extends AbstractPlugin implements LoggerAwareInterface, HookableCollection, Activateable {
	use LoggerAwareTrait;
	use HookableParent;

	/**
	 * Name of the Quick Cross-Sells custom database relation table.
	 */
	const PLUGIN_DB_TABLE_NAME = 'wcqcs_quick_cross_sells';

	/**
	 * Path of the main plugin directory.
	 *
	 * @var string
	 */
	protected $plugin_dir = '';

	/**
	 * Current version of plugin.
	 *
	 * @var string
	 */
	protected $plugin_version = '';

	/**
	 * This stores the IDs of products that just has been added to cart.
	 *
	 * @var array
	 */
	protected $added_cross_sell_product_ids = array();

	/**
	 * Plugin constructor.
	 *
	 * @param WPDesk_Plugin_Info $plugin_info Plugin info.
	 */
	public function __construct( WPDesk_Plugin_Info $plugin_info ) {
		parent::__construct( $plugin_info );
		$this->setLogger( new NullLogger() );

		$this->plugin_url       = $this->plugin_info->get_plugin_url();
		$this->plugin_namespace = $this->plugin_info->get_text_domain();
		$this->plugin_dir       = $this->plugin_info->get_plugin_dir();
		$this->plugin_version   = $this->plugin_info->get_version();

		$this->docs_url     = esc_url( __( 'https://www.wpdesk.net/docs/quick-cross-sells-woocommerce/', 'woocommerce-quick-cross-sells' ) );
		$this->settings_url = admin_url( 'admin.php?page=wc-settings&tab=products&section=woocommerce-quick-cross-sells' );
	}

	/**
	 * Initializes plugin external state.
	 *
	 * The plugin internal state is initialized in the constructor and the plugin should be internally consistent after creation.
	 * The external state includes hooks execution, communication with other plugins, integration with WC etc.
	 *
	 * @return void
	 */
	public function init() {
		parent::init();

		require_once trailingslashit( $this->plugin_dir ) . 'src/Plugin/wcqcs-quick-cross-sell-functions.php';
	}

	/**
	 * Integrate with WordPress and with other plugins using action/filter system.
	 *
	 * @return void
	 */
	public function hooks() {
		parent::hooks();

		// Custom Data Store.
		add_filter( 'woocommerce_data_stores', array( $this, 'register_woocommerce_custom_data_store' ), 10, 1 );

		// Plugin settings.
		add_filter( 'woocommerce_get_sections_products', array( $this, 'register_woocommerce_settings_wcqcs_section' ), 10, 1 );
		add_filter( 'woocommerce_get_settings_products', array( $this, 'add_plugin_settings_fields' ), 10, 2 );

		// Product options.
		add_action( 'woocommerce_product_options_related', array( $this, 'display_product_quick_cross_sell_options' ), 10, 0 );
		add_action( 'woocommerce_process_product_meta', array( $this, 'save_product_quick_cross_sells_options' ), 10, 1 );

		// Product frontend section.
		add_action( 'woocommerce_before_add_to_cart_button', array( $this, 'display_product_quick_cross_sells_section' ), 90, 0 );

		// Custom AJAX product search action.
		add_action( 'wp_ajax_wcqcs_json_search_products', array( $this, 'handle_wp_ajax_json_custom_search_products' ), 10, 0 );

		// Adding products to cart.
		add_action( 'woocommerce_add_to_cart', array( $this, 'maybe_add_cross_sells_to_cart' ), 10, 2 );
		add_filter( 'wc_add_to_cart_message_html', array( $this, 'append_cross_sells_to_added_to_cart_notice' ), 10, 1 );

		// Cart discounts.
		add_action( 'woocommerce_before_calculate_totals', array( $this, 'calculate_cart_discounts' ), 10, 1 );
		add_filter( 'woocommerce_cart_item_price', array( $this, 'display_cart_item_price_discounted_price' ), 10, 3 );
		add_filter( 'woocommerce_subscriptions_calculated_total', array( $this, 'recalculate_cart_discounts_after_subscriptions_calculated_total' ), 10, 1 );

		// Storing item's added with product IDs.
		add_action( 'woocommerce_remove_cart_item', array( $this, 'update_cart_items_added_with_product_ids' ), 10, 2 );
		add_action( 'woocommerce_checkout_create_order_line_item', array( $this, 'save_order_item_added_with_product_ids' ), 10, 4 );
		add_filter( 'woocommerce_hidden_order_itemmeta', array( $this, 'hide_order_itemmeta_added_with_product_id' ), 10, 1 );

		// Delete all product's Quick Cross-Sell relations on product post delete.
		add_action( 'delete_post', array( $this, 'delete_product_relations' ), 10, 1 );
		add_action( 'woocommerce_delete_product', array( $this, 'delete_product_relations' ), 10, 1 );
		add_action( 'woocommerce_delete_product_variation', array( $this, 'delete_product_relations' ), 10, 1 );
		add_action( 'wp_trash_post', array( $this, 'delete_subscription_variation_relations' ), 10, 1 );
	}

	/**
	 * Plugin activated in WordPress.
	 *
	 * @return void
	 */
	public function activate() {
		$this->create_db_table();
	}

	/**
	 * Create custom DB table on plugin activation.
	 *
	 * @return void
	 */
	private function create_db_table() {
		global $wpdb;

		$charset_collate = $wpdb->get_charset_collate();

		$sql = "CREATE TABLE {$wpdb->prefix}" . self::PLUGIN_DB_TABLE_NAME . " (
				relation_id bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
				parent_product_id bigint(20) UNSIGNED NOT NULL,
				product_id bigint(20) UNSIGNED NOT NULL,
				product_discount decimal(10,5) DEFAULT NULL,
				product_discount_type varchar(100) NOT NULL DEFAULT 'none',
				product_cta longtext,
				product_order bigint(20) UNSIGNED NOT NULL,
				PRIMARY KEY (relation_id),
				UNIQUE KEY relation (parent_product_id, product_id)
		) $charset_collate;";

		require_once ABSPATH . 'wp-admin/includes/upgrade.php';
		dbDelta( $sql );

		add_option( 'wcqcs_db_version', $this->plugin_version );
	}

	/**
	 * Append JS scripts in the WordPress admin panel.
	 *
	 * @return void
	 */
	public function admin_enqueue_scripts() {
		$screen    = get_current_screen();
		$screen_id = $screen ? $screen->id : '';

		if ( in_array( $screen_id, array( 'product', 'edit-product' ), true ) ) {
			wp_enqueue_style( 'wcqcs', esc_url( $this->get_plugin_assets_url() . 'css/admin/product.min.css' ), array(), $this->plugin_version );
			wp_register_script( 'wcqcs-product', esc_url( $this->get_plugin_assets_url() . 'js/admin/product.min.js' ), array( 'jquery', 'jquery-ui-sortable' ), $this->plugin_version, true );
		}
	}

	/**
	 * Append JS scripts in WordPress.
	 *
	 * @return void
	 */
	public function wp_enqueue_scripts() {

		if ( is_product() ) {
			wp_enqueue_style( 'wcqcs', esc_url( $this->get_plugin_assets_url() . 'css/front/product.min.css' ), array(), $this->plugin_version );
		}

	}

	/**
	 * Register custom Woocommerce Data Store for Quick Cross-Sells.
	 *
	 * @param array $stores WooCommerce Data Stores.
	 *
	 * @return array
	 */
	public function register_woocommerce_custom_data_store( $stores ) {
		$stores['wcqcs-quick-cross-sell'] = '\WPDesk\QuickCrossSells\QuickCrossSellDataStore';

		return $stores;
	}

	/**
	 * Register WooCommerce Settings Product tab WCQCS section.
	 *
	 * @param array $sections WooCommerce settings sections.
	 *
	 * @return array
	 */
	public function register_woocommerce_settings_wcqcs_section( $sections ) {
		$sections['woocommerce-quick-cross-sells'] = __( 'Quick Cross-Sells WooCommerce', 'woocommerce-quick-cross-sells' );

		return $sections;
	}

	/**
	 * Add setting fields to WCQCS section of the WooCommerce Settings.
	 *
	 * @param array  $settings WooCommerce settings.
	 * @param string $current_section Current settings section.
	 *
	 * @return array
	 */
	public function add_plugin_settings_fields( $settings = array(), $current_section ) {
		if ( 'woocommerce-quick-cross-sells' !== $current_section ) {
			return $settings;
		}

		$settings = array(
			array(
				'id'   => 'wcqcs_settings',
				'name' => __( 'Quick Cross-Sells WooCommerce', 'woocommerce-quick-cross-sells' ),
				'type' => 'title',
				'desc' => sprintf( '<a href="%s" target="_blank">%s</a>', $this->docs_url, __( 'View plugin documentation', 'woocommerce-quick-cross-sells' ) . ' &rarr;' ),
			),
			array(
				'id'          => 'wcqcs_section_default_title',
				'name'        => __( 'Quick Cross-Sells section default title', 'woocommerce-quick-cross-sells' ),
				'type'        => 'text',
				'desc'        => __( 'This will be used on all product pages by default. You can also set it per each product individually.', 'woocommerce-quick-cross-sells' ),
				'default'     => \wcqcs_get_default_wcq_section_title_value(),
				/* translators: %s: default section title */
				'placeholder' => sprintf( __( 'e.g. %s&hellip;', 'woocommerce-quick-cross-sells' ), \wcqcs_get_default_wcq_section_title_value() ),
			),
			array(
				'id'      => 'wcqcs_show_images',
				'name'    => __( 'Product images', 'woocommerce-quick-cross-sells' ),
				'type'    => 'checkbox',
				'desc'    => __( 'Show product images', 'woocommerce-quick-cross-sells' ),
				'default' => 'yes',
			),
			array(
				'id'      => 'wcqcs_allow_discounts_with_coupons',
				'name'    => __( 'Enable discounts with coupons', 'woocommerce-quick-cross-sells' ),
				'type'    => 'checkbox',
				'desc'    => __( 'Quick Cross-Sells discounts may be granted even if there are applied coupons in the cart', 'woocommerce-quick-cross-sells' ),
				'default' => 'no',
			),
			array(
				'id'   => 'wcqcs_settings',
				'type' => 'sectionend',
			),
		);

		return $settings;

	}

	/**
	 * Display Quick Cross-Sells options in product's Related tab.
	 *
	 * @return void
	 */
	public function display_product_quick_cross_sell_options() {
		global $post, $product_object;

		if ( ! in_array( $product_object->get_type(), array( 'simple', 'variable', 'subscription', 'variable-subscription' ), true ) ) {
			return;
		}

		$quick_cross_sell_products = array();

		$cross_sells = \wcqcs_get_quick_cross_sells_by_product_id( $product_object->get_id(), 'parent' );

		if ( false !== $cross_sells && ! empty( $cross_sells ) ) {
			foreach ( (array) $cross_sells as $cross_sell ) {
				$cross_sell_product = wc_get_product( $cross_sell->get_product_id() );
				if ( ! $cross_sell_product ) {
					continue;
				}

				$quick_cross_sell_products[] = array(
					'row_key'               => $cross_sell->get_id(),
					'product_id'            => $cross_sell->get_product_id(),
					'product_name'          => wcqcs_format_product_name( $cross_sell_product->get_formatted_name() ),
					'product_cta'           => $cross_sell->get_product_cta(),
					'product_discount_type' => $cross_sell->get_product_discount_type(),
					'product_discount'      => \wcqcs_format_discount_amount( $cross_sell->get_product_discount() ),
				);
			}
		}

		wp_localize_script(
			'wcqcs-product',
			'wcqcsProductQuickCrossSells',
			array(
				'products' => $quick_cross_sell_products,
				'strings'  => array(
					'i18n_remove_product_row' => __( 'Are you sure you want to remove this product from Quick Cross-Sells?', 'woocommerce-quick-cross-sells' ),
				),
			)
		);

		wp_enqueue_script( 'wcqcs-product' );

		include trailingslashit( $this->plugin_dir ) . 'views/admin/product-quick-cross-sells-options.php';
		include trailingslashit( $this->plugin_dir ) . 'views/admin/tmpl-wcqcs-product-row.php';

	}

	/**
	 * Handle single product Quick Cross-Sells options save action.
	 *
	 * @param int $product_id Product ID.
	 *
	 * @return void
	 */
	public function save_product_quick_cross_sells_options( $product_id = 0 ) {
		if ( isset( $_POST['woocommerce_meta_nonce'] ) ) {
			// phpcs:ignore
			$nonce = strval( wc_clean( wp_unslash( $_POST['woocommerce_meta_nonce'] ) ) );
			if ( ! wp_verify_nonce( $nonce, 'woocommerce_save_data' ) ) {
				return;
			}
		} else {
			return;
		}

		if ( ! isset( $_POST['_wcqcs_section_title'] ) && ! isset( $_POST['_wcqcs_products'] ) ) {
			return;
		}

		$product = wc_get_product( $product_id );
		if ( ! $product ) {
			return;
		}

		// Save WCQCS section title.
		if ( isset( $_POST['_wcqcs_section_title'] ) ) {

			// phpcs:ignore
			$section_title = strval( wc_clean( wp_unslash( $_POST['_wcqcs_section_title'] ) ) );

			$product->update_meta_data( '_wcqcs_section_title', $section_title );
			$product->save();

		}

		// Save WCQCS products.
		$valid_relation_ids = array();
		if ( isset( $_POST['_wcqcs_products'] ) ) {

			// phpcs:ignore
			$changes = wc_clean( wp_unslash( $_POST['_wcqcs_products'] ) );
			if ( is_array( $changes ) && ! empty( $changes ) ) {

				$cross_sell_i = 0;
				foreach ( $changes as $cross_sell_row_key => $cross_sell ) {

					if ( isset( $cross_sell['product_id'] ) && $cross_sell['product_id'] && wc_get_product( $cross_sell['product_id'] ) ) {

						$cross_sell_i++;

						if ( isset( $cross_sell['is_new_row'] ) && 1 === (int) $cross_sell['is_new_row'] ) {
							$relation = new QuickCrossSellRelation();
						} else {
							$relation = new QuickCrossSellRelation( (int) $cross_sell_row_key );
						}

						$relation->set_product_id( (int) $cross_sell['product_id'] );
						$relation->set_parent_product_id( (int) $product_id );

						$discount_amount = \wcqcs_format_discount_amount( $cross_sell['product_discount'] );

						// If there's no discount amount, set its type to 'none'.
						if ( 0 >= (float) $discount_amount ) {
							$cross_sell['product_discount_type'] = 'none';
						}

						// If discount has type 'none', set its value to '0'.
						if ( 'none' === $cross_sell['product_discount_type'] ) {
							$discount_amount = \wcqcs_format_discount_amount( '0' );
						}

						$relation->set_product_discount_type( $cross_sell['product_discount_type'] );
						$relation->set_product_discount( $discount_amount );

						$relation->set_product_cta( $cross_sell['product_cta'] );
						$relation->set_product_order( $cross_sell_i );

						$relation->save();

						$valid_relation_ids[] = $relation->get_id();

					}
				}
			}
		}

		// Remove all previous Quick Cross-Sells relations of this parent product (now deleted or invalid).
		\wcqcs_delete_quick_cross_sells_by_product_id( $product_id, 'parent', $valid_relation_ids );
	}

	/**
	 * Display frontend product Quick Cross-Sells section.
	 *
	 * @return void
	 */
	public function display_product_quick_cross_sells_section() {
		global $product;
		if ( ! $product ) {
			return;
		}

		if ( ! in_array( $product->get_type(), array( 'simple', 'variable', 'subscription', 'variable-subscription' ), true ) ) {
			return;
		}

		// Get cross-sells product ids.
		$quick_cross_sell_product_ids = array();
		$cross_sells                  = \wcqcs_get_quick_cross_sells_by_product_id( $product->get_id(), 'parent' );
		if ( false !== $cross_sells && ! empty( $cross_sells ) ) {
			foreach ( (array) $cross_sells as $cross_sell ) {
				$quick_cross_sell_product_ids[] = $cross_sell->get_product_id();
			}
		}

		if ( empty( $quick_cross_sell_product_ids ) ) {
			return;
		}

		// Do not display current product.
		$quick_cross_sell_product_ids = array_diff( $quick_cross_sell_product_ids, array( $product->get_id() ) );

		// Do not display current product's variations.
		if ( in_array( $product->get_type(), array( 'variable', 'variable-subscription' ) ) ) {
			$quick_cross_sell_product_ids = array_diff( $quick_cross_sell_product_ids, $product->get_children() );
		}

		if ( empty( $quick_cross_sell_product_ids ) ) {
			return;
		}

		$quick_cross_sell_product_args = array(
			'type'         => array( 'simple', 'variation', 'subscription' ),
			'stock_status' => array( 'instock', 'onbackorder' ),
			'include'      => $quick_cross_sell_product_ids,
			'limit'        => -1,
			'orderby'      => 'post__in',
			'order'        => 'ASC',
		);
		$quick_cross_sell_products     = wc_get_products( $quick_cross_sell_product_args );

		if ( empty( $quick_cross_sell_products ) ) {
			return;
		}

		$cross_sells_section_title = \wcqcs_get_product_section_title( $product->get_id() );

		include trailingslashit( $this->plugin_dir ) . 'views/front/single-product-quick-cross-sells.php';
	}

	/**
	 * Handle custom AJAX product search action (searching simple products, variations, simple subscriptions and subscription variations only).
	 *
	 * @return void
	 */
	public function handle_wp_ajax_json_custom_search_products() {
		check_ajax_referer( 'search-products', 'security' );

		$term = '';

		if ( isset( $_GET['term'] ) ) {
			// phpcs:ignore
			$term = strval( wc_clean( wp_unslash( $_GET['term'] ) ) );
		}

		if ( empty( $term ) ) {
			wp_die();
		}

		if ( ! empty( $_GET['limit'] ) ) {
			$limit = absint( $_GET['limit'] );
		} else {
			$limit = absint( apply_filters( 'woocommerce_json_search_limit', 30 ) );
		}

		$exclude_ids = array();

		if ( isset( $_GET['exclude'] ) && ! empty( $_GET['exclude'] ) ) {

			$exclude = array_map( 'absint', (array) wp_unslash( $_GET['exclude'] ) );
			foreach ( $exclude as $exclude_product_id ) {

				$exclude_ids[] = $exclude_product_id;

				// Exclude also product variations.
				$exclude_product = wc_get_product( $exclude_product_id );
				if ( $exclude_product && in_array( $exclude_product->get_type(), array( 'variable', 'variable-subscription' ) ) ) {
					$exclude_ids = array_merge( $exclude_ids, $exclude_product->get_children() );
				}
			}
			$exclude_ids = array_filter( array_unique( $exclude_ids ) );
		}

		$product_args    = array(
			'type'    => array( 'simple', 'variation', 'subscription' ),
			'exclude' => $exclude_ids,
			'limit'   => $limit,
			's'       => $term,
		);
		$product_objects = (array) wc_get_products( $product_args );

		$product_objects = array_filter( $product_objects, 'wc_products_array_filter_readable' );
		$products        = array();

		foreach ( $product_objects as $product_object ) {
			$formatted_name = wcqcs_format_product_name( $product_object->get_formatted_name() );

			$products[ $product_object->get_id() ] = rawurldecode( $formatted_name );
		}

		wp_send_json( apply_filters( 'woocommerce_json_search_found_products', $products ) );
	}

	/**
	 * Handle add-to-cart selected Quick Cross-Sells products with parent product action.
	 *
	 * @param string $added_with_cart_item_key Cart item key.
	 * @param int    $added_with_product_id Cart item product ID.
	 *
	 * @return void
	 */
	public function maybe_add_cross_sells_to_cart( $added_with_cart_item_key = '', $added_with_product_id = 0 ) {
		if ( is_admin() && ! wp_doing_ajax() ) {
			return;
		}

		// phpcs:ignore
		$requested_items = isset( $_REQUEST['wcqcs-add-to-cart'] ) && is_array( $_REQUEST['wcqcs-add-to-cart'] ) ? (array) wp_unslash( $_REQUEST['wcqcs-add-to-cart'] ) : array();

		if ( empty( $requested_items ) ) {
			return;
		}

		// Make sure that $added_product_ids is emptied.
		$this->added_cross_sell_product_ids = array();

		// Make sure that Quick Cross-Sells will be added to cart only once in single request.
		remove_action( 'woocommerce_add_to_cart', array( $this, 'maybe_add_cross_sells_to_cart' ), 10 );

		$added_product_ids = array();

		foreach ( $requested_items as $cross_sell_product_id ) {
			$cross_sell_product_id = absint( wp_unslash( $cross_sell_product_id ) );

			$adding_to_cart = wc_get_product( $cross_sell_product_id );
			if ( ! $adding_to_cart ) {
				continue;
			}

			$added = false;

			if ( in_array( $adding_to_cart->get_type(), array( 'simple', 'subscription' ) ) ) {

				$added = WC()->cart->add_to_cart( $cross_sell_product_id, 1 );

			} elseif ( in_array( $adding_to_cart->get_type(), array( 'variation', 'subscription_variation' ) ) ) {

				$cross_sell_variation_id = $cross_sell_product_id;

				$cross_sell_variation = wc_get_product( $cross_sell_variation_id );
				if ( $cross_sell_variation ) {
					$cross_sell_product_id           = $cross_sell_variation->get_parent_id();
					$cross_sell_variation_attributes = wc_get_product_variation_attributes( $cross_sell_variation_id );

					$added = WC()->cart->add_to_cart( $cross_sell_product_id, 1, $cross_sell_variation_id, $cross_sell_variation_attributes );
				}
			}

			if ( false !== $added ) {

				$added_product_ids[] = $adding_to_cart->get_id();

				// Store parent's product_id as cart item meta.
				$added_with_product_ids = array();

				$cart_item_key = $added;
				$cart          = WC()->cart->get_cart();

				if ( isset( $cart[ $cart_item_key ]['wcqcs_added_with_product_ids'] ) ) {
					$added_with_product_ids = (array) $cart[ $cart_item_key ]['wcqcs_added_with_product_ids'];
				}
				$added_with_product_ids[] = (int) $added_with_product_id;
				$added_with_product_ids   = array_filter( array_unique( $added_with_product_ids ) );

				// Update cart.
				WC()->cart->cart_contents[ $cart_item_key ]['wcqcs_added_with_product_ids'] = $added_with_product_ids;
				WC()->cart->set_session();

			}
		}

		if ( ! empty( $added_product_ids ) ) {
			$this->added_cross_sell_product_ids = $added_product_ids;
		}

	}

	/**
	 * Append product's add-to-cart success notice with selected Quick Cross-Sells product titles.
	 *
	 * @param string $message Message content.
	 *
	 * @return string
	 */
	public function append_cross_sells_to_added_to_cart_notice( $message = '' ) {
		// phpcs:ignore
		$requested_items = isset( $_REQUEST['wcqcs-add-to-cart'] ) && is_array( $_REQUEST['wcqcs-add-to-cart'] ) ? (array) wp_unslash( $_REQUEST['wcqcs-add-to-cart'] ) : array();

		if ( empty( $requested_items ) ) {
			return $message;
		}

		// Get IDs of products that just has been succesfully added to cart during this request.
		$added_product_ids = $this->added_cross_sell_product_ids;
		if ( empty( $added_product_ids ) ) {
			return $message;
		}

		$cross_sell_titles = array();
		foreach ( $added_product_ids as $cross_sell_product_id ) {
			$cross_sell_product = wc_get_product( $cross_sell_product_id );
			if ( $cross_sell_product ) {

				$cross_sell_title = apply_filters( 'woocommerce_add_to_cart_qty_html', '1 &times; ', $cross_sell_product_id );

				$item_name_in_quotes = sprintf(
					/* translators: %s: product name */
					_x( '&ldquo;%s&rdquo;', 'Item name in quotes', 'woocommerce-quick-cross-sells' ),
					wp_strip_all_tags( $cross_sell_product->get_title() )
				);

				$cross_sell_title .= apply_filters( 'woocommerce_add_to_cart_item_name_in_quotes', $item_name_in_quotes, $cross_sell_product_id );

				$cross_sell_titles[] = $cross_sell_title;
			}
		}

		if ( ! empty( $cross_sell_titles ) ) {
			$message = rtrim( $message, ' ' );
			/* translators: %s: products' names */
			$message .= ' (' . sprintf( __( 'Together with %s', 'woocommerce-quick-cross-sells' ), wc_format_list_of_items( $cross_sell_titles ) ) . ').';
		}

		// We do not no longer need $added_product_ids array contents, so make sure that it is emptied.
		$this->added_cross_sell_product_ids = array();

		return $message;
	}

	/**
	 * Calculate Quick Cross-Sells discounts on Cart update.
	 *
	 * @param \WC_Cart $cart_object Cart object.
	 *
	 * @return void
	 */
	public function calculate_cart_discounts( $cart_object ) {

		if ( $cart_object->is_empty() ) {
			return;
		}

		$cart = $cart_object->get_cart();

		// Clear all previous cart discounts.
		foreach ( $cart as $cart_item_key => $cart_item ) {

			if ( isset( $cart[ $cart_item_key ]['wcqcs_discount_applied'] ) ) {

				$_product = wc_get_product( $cart_item['data']->get_id() );
				if ( $_product ) {
					$cart_object->cart_contents[ $cart_item_key ]['data']->set_price( $_product->get_price() );
				}

				if ( isset( $cart[ $cart_item_key ]['wcqcs_discounted_price'] ) ) {
					unset( $cart_object->cart_contents[ $cart_item_key ]['wcqcs_discounted_price'] );
				}

				unset( $cart_object->cart_contents[ $cart_item_key ]['wcqcs_discount_applied'] );

			}
		}

		// Do not apply Quick Cross-Sells discounts for recurring carts made by WooCommerce Subscriptions plugin.
		if ( isset( $cart_object->recurring_cart_key ) && $cart_object->recurring_cart_key ) {
			return;
		}

		// Check if Quick Cross-Sells discounts are allowed to work with coupons applied in cart.
		if ( 'yes' !== get_option( 'wcqcs_allow_discounts_with_coupons' ) ) {
			if ( ! empty( $cart_object->get_applied_coupons() ) ) {
				return;
			}
		}

		$cart_item_object_ids          = array();
		$cart_item_parent_product_qtys = array();

		// Get all cart items' object_ids, product_ids and its quantities.
		foreach ( $cart as $cart_item_key => $cart_item ) {

			$cart_item_product_id = $cart_item['product_id'];

			// Store item object_id (product_id or variation_id if provided).
			$cart_item_object_id    = ( isset( $cart_item['variation_id'] ) && $cart_item['variation_id'] ) ? $cart_item['variation_id'] : $cart_item['product_id'];
			$cart_item_object_ids[] = (int) $cart_item_object_id;

			// Store item quantity by item parent_id (always use here product_id instead of varitaion_id).
			if ( ! isset( $cart_item_parent_product_qtys[ $cart_item_product_id ] ) ) {
				$cart_item_parent_product_qtys[ $cart_item_product_id ] = (int) 0;
			}
			$cart_item_parent_product_qtys[ $cart_item_product_id ] += (int) $cart_item['quantity'];

		}
		if ( empty( $cart_item_parent_product_qtys ) ) {
			return;
		}

		$cart_item_object_ids = array_filter( array_unique( $cart_item_object_ids ) );
		$cart_item_parent_ids = array_keys( $cart_item_parent_product_qtys );

		// Check Quick Cross-Sells relations for all cart item products as possible parent_products.
		$cart_available_discounts = array();
		foreach ( $cart_item_parent_ids as $parent_product_id ) {
			$cart_item_product_relations = \wcqcs_get_quick_cross_sells_by_product_id( absint( $parent_product_id ), 'parent', false );

			if ( false === $cart_item_product_relations || empty( $cart_item_product_relations ) ) {
				continue;
			}

			// Loop through all parent_product relations.
			foreach ( (array) $cart_item_product_relations as $cart_item_relation ) {

				// Check if there are products in cart (simple, variation, simple subscription or subscription variation) that belongs to this parent_product relation.
				$product_id = (int) $cart_item_relation->get_product_id();
				if ( ! in_array( $product_id, $cart_item_object_ids, true ) ) {
					continue;
				}

				// Check if the relation grants a discount for the product.
				if ( 0 >= $cart_item_relation->get_product_discount() ) {
					continue;
				}

				// Calculate this relation's discounted product price and store it.
				$cart_available_discounts[ $product_id ][ $parent_product_id ] = $cart_item_relation->get_discounted_price();

			}
		}

		if ( empty( $cart_available_discounts ) ) {
			return;
		}

		// Leave only one, the most profitable parent-product discount when two-way discounts are available.
		if ( ! apply_filters( 'wcqcs_allow_two_way_discounts', false ) ) {

			foreach ( $cart_available_discounts as $product_id => $product_discounts ) {
				foreach ( $product_discounts as $parent_product_id => $discounted_product_price ) {

					// Find the other-way discount.
					if ( isset( $cart_available_discounts[ $parent_product_id ][ $product_id ] ) ) {

						$discounted_parent_product_price = $cart_available_discounts[ $parent_product_id ][ $product_id ];

						// Calculate products' discount amounts.
						$product_discount_amount        = wcqcs_get_product_calculated_discount_amount( (int) $product_id, $discounted_product_price );
						$parent_product_discount_amount = wcqcs_get_product_calculated_discount_amount( (int) $parent_product_id, $discounted_parent_product_price );

						if ( $product_discount_amount > $parent_product_discount_amount ) {

							// Remove the other-way discount if it's less profitable than this one.
							unset( $cart_available_discounts[ $parent_product_id ][ $product_id ] );

						} elseif ( $product_discount_amount === $parent_product_discount_amount ) {

							// If discount amounts are the same, make sure to leave only one available.
							unset( $cart_available_discounts[ $product_id ][ $parent_product_id ] );

						}
					}
				}
			}

			// Clear products with no available discounts left.
			foreach ( $cart_available_discounts as $product_id => $product_discounts ) {
				if ( empty( $product_discounts ) ) { // @phpstan-ignore-line
					unset( $cart_available_discounts[ $product_id ] );
				}
			}

			if ( empty( $cart_available_discounts ) ) {
				return;
			}
		}

		// Apply discounts if there's any.
		foreach ( $cart as $cart_item_key => $cart_item ) {
			// Do not apply Quick Cross-Sells discounts for WooCommerce Subscriptions renewal items.
			if ( isset( $cart_item['subscription_renewal'] ) ) {
				continue;
			}

			$cart_item_product_price = (float) $cart_item['data']->get_price();

			// Get price without sign up fee if it's one of the WooCommerce Subscriptions product types.
			if ( class_exists( 'WC_Subscriptions_Product' ) && \WC_Subscriptions_Product::is_subscription( $cart_item['data'] ) ) {
				$cart_item_product_price = (float) \WC_Subscriptions_Product::get_price( $cart_item['data'] );
			}

			// Check if the item isn't already for free.
			if ( $cart_item_product_price <= 0 ) {
				continue;
			}

			// Get product_id or variation_id (if provided).
			$cart_item_product_id = $cart_item['data']->get_id();

			// Check if there are discounts available to apply for this product.
			if ( ! isset( $cart_available_discounts[ $cart_item_product_id ] ) ) {
				continue;
			}

			// Sort available discounts by its profitability to the customer.
			$cart_item_available_discounts = $cart_available_discounts[ $cart_item_product_id ];
			asort( $cart_item_available_discounts );

			$cart_item_qty      = (int) $cart_item['quantity'];
			$cart_item_qty_left = $cart_item_qty;

			$discounted_cart_item_line_subtotal = 0;

			// Loop through all available discounts and calculate the cart item total until its quantity will be maxed.
			foreach ( $cart_item_available_discounts as $parent_product_id => $discounted_product_price ) {

				if ( ! isset( $cart_item_parent_product_qtys[ $parent_product_id ] ) ) {
					return;
				}
				$parent_product_qty_left = $cart_item_parent_product_qtys[ $parent_product_id ];

				while ( $parent_product_qty_left > 0 ) {

					if ( 0 === $cart_item_qty_left ) {
						break;
					}

					$discounted_cart_item_line_subtotal += $discounted_product_price;
					$parent_product_qty_left--;
					$cart_item_qty_left--;

				}
			}

			// If there is any quantity left, offer it by its product's default price.
			if ( $cart_item_qty_left > 0 ) {
				$discounted_cart_item_line_subtotal += ( $cart_item_qty_left * $cart_item_product_price );
				$cart_item_qty_left                  = 0;
			}

			// Calculate discounted price based on cart item quantity.
			$discounted_cart_item_product_price = max( 0, ( $discounted_cart_item_line_subtotal / $cart_item_qty ) );

			// Check if the discounted price is really lower than the default price.
			if ( $discounted_cart_item_product_price < $cart_item_product_price ) {

				// Set discounted price.
				$cart_object->cart_contents[ $cart_item_key ]['data']->set_price( $discounted_cart_item_product_price );

				// Store discounted price in cart item.
				$cart_object->cart_contents[ $cart_item_key ]['wcqcs_discounted_price'] = $discounted_cart_item_product_price;

				// Mark cart item as discount applied.
				$cart_object->cart_contents[ $cart_item_key ]['wcqcs_discount_applied'] = 1;

			}
		}

	}

	/**
	 * Display cart item's discounted price in Cart table (with regular price formatted as sale price).
	 *
	 * @param string $price Item price.
	 * @param array  $cart_item Cart Item array.
	 * @param string $cart_item_key Cart Item key.
	 *
	 * @return string
	 */
	public function display_cart_item_price_discounted_price( $price = '', $cart_item = array(), $cart_item_key = '' ) {
		if ( ! isset( $cart_item['wcqcs_discount_applied'] ) || 1 !== $cart_item['wcqcs_discount_applied'] ) {
			return $price;
		}

		$_product = wc_get_product( $cart_item['data']->get_id() );
		if ( $_product ) {

			// Get cart item's Product regular price as the regular price.
			$_product_price_args = array(
				'price' => (float) $_product->get_regular_price(),
			);

			if ( WC()->cart->display_prices_including_tax() ) {
				$_product_price = wc_get_price_including_tax( $_product, $_product_price_args );
			} else {
				$_product_price = wc_get_price_excluding_tax( $_product, $_product_price_args );
			}

			$_product_price = wc_price( (float) $_product_price );

			// Get cart item's stored discounted price as the sale price.
			$discounted_price = $price;

			if ( isset( $cart_item['wcqcs_discounted_price'] ) ) {
				$discounted_price_args = array(
					'price' => (float) $cart_item['wcqcs_discounted_price'],
				);

				if ( WC()->cart->display_prices_including_tax() ) {
					$discounted_price = wc_get_price_including_tax( $_product, $discounted_price_args );
				} else {
					$discounted_price = wc_get_price_excluding_tax( $_product, $discounted_price_args );
				}

				$discounted_price = wc_price( (float) $discounted_price );
			}

			$price = wc_format_sale_price( $_product_price, $discounted_price );

			// Include the details of the subscription if it's one of the WooCommerce Subscriptions product types.
			if ( class_exists( 'WC_Subscriptions_Product' ) && \WC_Subscriptions_Product::is_subscription( $_product ) ) {
				$price = \WC_Subscriptions_Product::get_price_string( $_product, array( 'price' => $price ) );
			}
		}

		return $price;
	}

	/**
	 * Recalculate Quick Cross-Sells discounts after WooCommerce Subscriptions Cart total has been calculated.
	 *
	 * @param float $total Cart total.
	 *
	 * @return float
	 */
	public function recalculate_cart_discounts_after_subscriptions_calculated_total( $total = 0 ) {
		$this->calculate_cart_discounts( WC()->cart );

		return $total;
	}

	/**
	 * Update cart items 'wcqcs_added_with_product_ids' meta when removing cart item.
	 *
	 * @param string   $cart_item_key Cart item key.
	 * @param \WC_Cart $cart Cart object.
	 *
	 * @return void
	 */
	public function update_cart_items_added_with_product_ids( $cart_item_key, $cart ) {
		$cart = WC()->cart->get_cart();

		$remove_product_id = (int) $cart[ $cart_item_key ]['product_id'];

		foreach ( $cart as $cart_item_key => $cart_item ) {

			if ( isset( $cart[ $cart_item_key ]['wcqcs_added_with_product_ids'] ) && is_array( $cart[ $cart_item_key ]['wcqcs_added_with_product_ids'] ) && in_array( $remove_product_id, $cart[ $cart_item_key ]['wcqcs_added_with_product_ids'], true ) ) {

				$added_with_product_ids = array_diff( (array) $cart[ $cart_item_key ]['wcqcs_added_with_product_ids'], array( $remove_product_id ) );

				// Update cart.
				if ( ! empty( $added_with_product_ids ) ) {
					WC()->cart->cart_contents[ $cart_item_key ]['wcqcs_added_with_product_ids'] = $added_with_product_ids;
				} else {
					unset( WC()->cart->cart_contents[ $cart_item_key ]['wcqcs_added_with_product_ids'] );
				}

				WC()->cart->set_session();

			}
		}
	}

	/**
	 * Save 'wcqcs_added_with_product_ids' cart item meta as '_wcqcs_added_with_product_id' order item metas.
	 *
	 * @param \WC_Order_Item_Product $item Order item object.
	 * @param string                 $cart_item_key Cart item key.
	 * @param array                  $values Cart item values.
	 * @param \WC_Order              $order Order object.
	 *
	 * @return void
	 */
	public function save_order_item_added_with_product_ids( $item, $cart_item_key, $values = array(), $order ) {
		// Do not save meta for items from WooCommerce Subscriptions object.
		if ( function_exists( 'wcs_is_subscription' ) && \wcs_is_subscription( $order ) ) {
			return;
		}

		if ( isset( $values['wcqcs_added_with_product_ids'] ) && is_array( $values['wcqcs_added_with_product_ids'] ) && ! empty( $values['wcqcs_added_with_product_ids'] ) ) {

			foreach ( $values['wcqcs_added_with_product_ids'] as $added_with_product_id ) {
				$item->add_meta_data( '_wcqcs_added_with_product_id', (string) $added_with_product_id, false );
			}
		}
	}

	/**
	 * Do not display '_wcqcs_added_with_product_id' order item meta.
	 *
	 * @param array $hidden Hidden order item meta keys.
	 *
	 * @return array
	 */
	public function hide_order_itemmeta_added_with_product_id( $hidden = array() ) {
		$hidden[] = '_wcqcs_added_with_product_id';

		return $hidden;
	}

	/**
	 * Delete all product's Quick Cross-Sell relations on product post delete.
	 *
	 * @param int $product_id Post ID.
	 *
	 * @return void
	 */
	public function delete_product_relations( $product_id ) {
		\wcqcs_delete_quick_cross_sells_by_product_id( $product_id, 'both' );
	}

	/**
	 * Delete all subscription variation's Quick Cross-Sell relations on product post trash.
	 *
	 * @param int $variation_id Post ID.
	 *
	 * @return void
	 */
	public function delete_subscription_variation_relations( $variation_id ) {
		$product = wc_get_product( $variation_id );

		if ( ! $product || 'subscription_variation' != $product->get_type() ) {
			return;
		}

		\wcqcs_delete_quick_cross_sells_by_product_id( $variation_id, 'both' );
	}
}
