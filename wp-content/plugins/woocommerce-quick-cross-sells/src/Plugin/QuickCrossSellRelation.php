<?php
/**
 * Represents a single Quick Cross-Sell relation.
 *
 * @package WPDesk\QuickCrossSells
 */

namespace WPDesk\QuickCrossSells;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * QuickCrossSellRelation class.
 *
 * @package WPDesk\QuickCrossSells
 */
class QuickCrossSellRelation extends \WC_Data {

	/**
	 * Quick Cross-Sell ID
	 *
	 * @var int|null
	 */
	protected $id = null; // @phpstan-ignore-line

	/**
	 * This is the name of this object type.
	 *
	 * @var string
	 */
	protected $object_type = 'wcqcs_quick_cross_sell';

	/**
	 * Quick Cross-Sell Data.
	 *
	 * @var array
	 */
	protected $data = array(
		'product_id'            => '',
		'parent_product_id'     => '',
		'product_discount'      => 0,
		'product_discount_type' => '',
		'product_cta'           => '',
		'product_order'         => 0,
	);

	/**
	 * Constructor for cross-sells.
	 *
	 * @param int $quick_cross_sell Quick Cross-Sell relation ID to load from the DB.
	 */
	public function __construct( $quick_cross_sell = null ) {
		parent::__construct();

		if ( is_numeric( $quick_cross_sell ) && ! empty( $quick_cross_sell ) ) {
			$this->set_id( $quick_cross_sell );
		} elseif ( 0 === $quick_cross_sell ) {
			$this->set_id( 0 );
		} else {
			$this->set_object_read( true );
		}

		$this->data_store = \WC_Data_Store::load( 'wcqcs-quick-cross-sell' );
		if ( false === $this->get_object_read() ) {
			$this->data_store->read( $this );
		}
	}

	/**
	 * --------------------------------------------------------------------------
	 * Getters
	 * --------------------------------------------------------------------------
	 */

	/**
	 * Returns the unique ID for this object.
	 *
	 * @return int|null
	 */
	public function get_id() {
		return $this->id;
	}

	/**
	 * Get Quick Cross-Sell product id.
	 *
	 * @param  string $context View or edit context.
	 * @return string
	 */
	public function get_product_id( $context = 'view' ) {
		return $this->get_prop( 'product_id', $context );
	}

	/**
	 * Get Quick Cross-Sell parent product id.
	 *
	 * @param  string $context View or edit context.
	 * @return int
	 */
	public function get_parent_product_id( $context = 'view' ) {
		return $this->get_prop( 'parent_product_id', $context );
	}

	/**
	 * Get Quick Cross-Sell product discount.
	 *
	 * @param  string $context View or edit context.
	 * @return int
	 */
	public function get_product_discount( $context = 'view' ) {
		return $this->get_prop( 'product_discount', $context );
	}

	/**
	 * Get Quick Cross-Sell product discount type.
	 *
	 * @param  string $context View or edit context.
	 * @return int
	 */
	public function get_product_discount_type( $context = 'view' ) {
		return $this->get_prop( 'product_discount_type', $context );
	}

	/**
	 * Get Quick Cross-Sell product CTA.
	 *
	 * @param  string $context View or edit context.
	 * @return int
	 */
	public function get_product_cta( $context = 'view' ) {
		return $this->get_prop( 'product_cta', $context );
	}

	/**
	 * Get Quick Cross-Sell product order.
	 *
	 * @param  string $context View or edit context.
	 * @return int
	 */
	public function get_product_order( $context = 'view' ) {
		return $this->get_prop( 'product_order', $context );
	}

	/**
	 * --------------------------------------------------------------------------
	 * Setters
	 * --------------------------------------------------------------------------
	 */

	/**
	 * Set Quick Cross-Sell product id.
	 *
	 * @param int $set Value to set.
	 *
	 * @return void
	 */
	public function set_product_id( $set ) {
		$this->set_prop( 'product_id', absint( $set ) );
	}

	/**
	 * Set Quick Cross-Sell parent product id.
	 *
	 * @param int $set Value to set.
	 *
	 * @return void
	 */
	public function set_parent_product_id( $set ) {
		$this->set_prop( 'parent_product_id', absint( $set ) );
	}

	/**
	 * Set Quick Cross-Sell parent product CTA.
	 *
	 * @param string $set Value to set.
	 *
	 * @return void
	 */
	public function set_product_cta( $set ) {
		$this->set_prop( 'product_cta', wc_clean( $set ) );
	}

	/**
	 * Set Quick Cross-Sell parent product discount.
	 *
	 * @param string $set Value to set.
	 *
	 * @return void
	 */
	public function set_product_discount( $set ) {
		$this->set_prop( 'product_discount', \wcqcs_format_discount_amount( $set ) );
	}

	/**
	 * Set Quick Cross-Sell parent product discount type.
	 *
	 * @param string $set Value to set.
	 *
	 * @return void
	 */
	public function set_product_discount_type( $set ) {
		$this->set_prop( 'product_discount_type', wc_clean( $set ) );
	}

	/**
	 * Set Quick Cross-Sell product order. Value to set.
	 *
	 * @param int $set Value to set.
	 *
	 * @return void
	 */
	public function set_product_order( $set ) {
		$this->set_prop( 'product_order', absint( $set ) );
	}

	/**
	 * --------------------------------------------------------------------------
	 * Other
	 * --------------------------------------------------------------------------
	 */

	/**
	 * Save Quick Cross-Sell data to the database.
	 *
	 * @return int|null
	 */
	public function save() {

		if ( ! is_a( $this->data_store, 'WC_Data_Store' ) || '\WPDesk\QuickCrossSells\QuickCrossSellDataStore' !== $this->data_store->get_current_class_name() ) {
			return $this->get_id();
		}

		/**
		 * Trigger action before saving to the DB. Allows you to adjust object props before save.
		 *
		 * @param \WC_Data          $this The object being saved.
		 * @param \WC_Data_Store $data_store THe data store persisting the data.
		 */
		do_action( 'woocommerce_before_' . $this->object_type . '_object_save', $this, $this->data_store );

		if ( null !== $this->get_id() ) {
			$this->data_store->update( $this );
		} else {
			$this->data_store->create( $this );
		}

		/**
		 * Trigger action after saving to the DB.
		 *
		 * @param \WC_Data          $this The object being saved.
		 * @param \WC_Data_Store $data_store THe data store persisting the data.
		 */
		do_action( 'woocommerce_after_' . $this->object_type . '_object_save', $this, $this->data_store );

		return $this->get_id();
	}


	/**
	 * Get Quick Cross-Sell relation discounted price.
	 *
	 * @return float|bool Product price or false if product doesn't exist.
	 */
	public function get_discounted_price() {

		$product = wc_get_product( $this->get_product_id() );
		if ( ! $product ) {
			return false;
		}

		$product_price = (float) $product->get_price();

		// Get price without sign up fee if it's one of the WooCommerce Subscriptions product types.
		if ( class_exists( 'WC_Subscriptions_Product' ) && \WC_Subscriptions_Product::is_subscription( $product ) ) {
			$product_price = (float) \WC_Subscriptions_Product::get_price( $product );
		}

		$product_discount = (float) $this->get_product_discount();

		if ( 0 >= $product_discount ) {
			return $product_price;
		}

		$product_discount_type = (string) $this->get_product_discount_type();
		if ( ! in_array( $product_discount_type, array( 'percent', 'fixed' ) ) ) {
			return $product_price;
		}

		switch ( $product_discount_type ) {
			case 'percent':
				$discounted_price = (float) max( 0, $product_price - ( ( $product_discount / 100 ) * $product_price ) );
				break;

			case 'fixed':
				$discounted_price = (float) max( 0, $product_price - $product_discount );
				break;

			default:
				$discounted_price = $product_price;
				return $discounted_price;
		}

		$discounted_price = wc_format_decimal( $discounted_price, '', true );
		$discounted_price = (float) min( $product_price, $discounted_price );

		return $discounted_price;

	}


	/**
	 * Get Quick Cross-Sell relation discounted price to display.
	 *
	 * @return string|bool Product price HTML or false if product doesn't exist.
	 */
	public function get_discounted_price_html() {

		$product = wc_get_product( $this->get_product_id() );
		if ( ! $product ) {
			return false;
		}

		if ( '' === $product->get_price() ) {

			$price = apply_filters( 'woocommerce_empty_price_html', '', $product );

		} else {

			$discounted_price = $this->get_discounted_price();
			$regular_price    = (float) $product->get_regular_price();

			if ( false !== $discounted_price && $regular_price !== $discounted_price ) {

				$price = wc_format_sale_price(
					(string) wc_get_price_to_display( $product, array( 'price' => $regular_price ) ),
					(string) wc_get_price_to_display( $product, array( 'price' => $discounted_price ) )
				) . $product->get_price_suffix();

				// Include the details of the subscription if it's one of the WooCommerce Subscriptions product types.
				if ( class_exists( 'WC_Subscriptions_Product' ) && \WC_Subscriptions_Product::is_subscription( $product ) ) {
					$price = \WC_Subscriptions_Product::get_price_string( $product, array( 'price' => $price ) );
				}
			} else {

				// If product is not discounted, return its default price.
				$price = $product->get_price_html();

			}
		}

		return $price;
	}

}
