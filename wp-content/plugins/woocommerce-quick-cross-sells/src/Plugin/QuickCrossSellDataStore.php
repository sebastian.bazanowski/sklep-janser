<?php
/**
 * Custom WooCommerce Data Store for Quick Cross-Sells.
 *
 * @package WPDesk\QuickCrossSells
 */

namespace WPDesk\QuickCrossSells;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * WCQCS Quick Cross-Sell Data Store.
 *
 * @package WPDesk\QuickCrossSells
 */
class QuickCrossSellDataStore extends \WC_Data_Store_WP implements \WC_Object_Data_Store_Interface {

	/**
	 * Method to create a new Quick Cross-Sell relation.
	 *
	 * @param QuickCrossSellRelation $relation Quick Cross-Sell relation object.
	 *
	 * @return void
	 */
	public function create( &$relation ) {
		global $wpdb;
		// phpcs:ignore
		$wpdb->insert(
			$wpdb->prefix . 'wcqcs_quick_cross_sells',
			array(
				'product_id'            => $relation->get_product_id(),
				'parent_product_id'     => $relation->get_parent_product_id(),
				'product_discount'      => $relation->get_product_discount(),
				'product_discount_type' => $relation->get_product_discount_type(),
				'product_cta'           => $relation->get_product_cta(),
				'product_order'         => $relation->get_product_order(),
			)
		);
		$relation->set_id( $wpdb->insert_id );
		$relation->apply_changes();
	}

	/**
	 * Update relation in the database.
	 *
	 * @param QuickCrossSellRelation $relation Quick Cross-Sell relation object.
	 *
	 * @return void
	 */
	public function update( &$relation ) {
		global $wpdb;
		if ( $relation->get_id() ) {
			// phpcs:ignore
			$wpdb->update(
				$wpdb->prefix . 'wcqcs_quick_cross_sells',
				array(
					'product_id'            => $relation->get_product_id(),
					'parent_product_id'     => $relation->get_parent_product_id(),
					'product_discount'      => $relation->get_product_discount(),
					'product_discount_type' => $relation->get_product_discount_type(),
					'product_cta'           => $relation->get_product_cta(),
					'product_order'         => $relation->get_product_order(),
				),
				array( 'relation_id' => $relation->get_id() )
			);
		}
		$relation->apply_changes();
	}

	/**
	 * Method to read a Quick Cross-Sell relation from the database.
	 *
	 * @param QuickCrossSellRelation $relation Quick Cross-Sell relation object.
	 * @throws \Exception If invalid data store.
	 *
	 * @return void
	 */
	public function read( &$relation ) {
		global $wpdb;

		$relation_data = false;

		if ( 0 !== $relation->get_id() ) {
			// phpcs:ignore
			$relation_data = $wpdb->get_row(
				$wpdb->prepare(
					"SELECT product_id, parent_product_id, product_order, product_discount, product_discount_type, product_cta FROM {$wpdb->prefix}wcqcs_quick_cross_sells WHERE relation_id = %d LIMIT 1",
					$relation->get_id()
				)
			);
		}

		if ( $relation_data ) {
			$relation->set_product_id( $relation_data->product_id );
			$relation->set_parent_product_id( $relation_data->parent_product_id );
			$relation->set_product_discount( $relation_data->product_discount );
			$relation->set_product_discount_type( $relation_data->product_discount_type );
			$relation->set_product_cta( $relation_data->product_cta );
			$relation->set_product_order( $relation_data->product_order );
			$relation->set_object_read( true );
			do_action( 'woocommerce_wcqcs_quick_cross_sell_loaded', $relation );
		} else {
			throw new \Exception( __( 'Invalid data store.', 'woocommerce-quick-cross-sells' ) );
		}
	}

	/**
	 * Deletes a Quick Cross-Sell relation from the database.
	 *
	 * @param  QuickCrossSellRelation $relation Quick Cross-Sell relation object.
	 * @param  array                  $args Array of args to pass to the delete method.
	 *
	 * @return bool result
	 */
	public function delete( &$relation, $args = array() ) {
		$relation_id = $relation->get_id();

		if ( ! $relation_id ) {
			return false;
		}

		global $wpdb;

		// Delete relation.
		// phpcs:ignore
		$wpdb->delete( $wpdb->prefix . 'wcqcs_quick_cross_sells', array( 'relation_id' => $relation_id ) );

		$relation->set_id( 0 );

		do_action( 'woocommerce_delete_wcqcs_quick_cross_sell', $relation_id );

		return true;
	}


	/**
	 * Return a relation id by its pair of product_id and parent_product_id.
	 *
	 * @param  int $parent_product_id ID of product parent.
	 * @param  int $product_id Id of the product.
	 *
	 * @return int An id of relation.
	 */
	public function get_relation_id_by_product_ids_pair( $parent_product_id = 0, $product_id = 0 ) {

		if ( ! $parent_product_id || ! $product_id ) {
			return 0;
		}

		global $wpdb;
		// phpcs:ignore
		return (int) $wpdb->get_var( $wpdb->prepare( "SELECT relation_id FROM {$wpdb->prefix}wcqcs_quick_cross_sells WHERE product_id = %d AND parent_product_id = %d LIMIT 1;", $product_id, $parent_product_id ) );

	}


	/**
	 * Return an ordered list of relations by product_id with its relation type (as parent, product or in both directions)
	 *
	 * @param  int    $product_id Id of the product.
	 * @param  string $relation_type Type of the relation to search for. Possible values: 'product', 'parent', 'both'.
	 *
	 * @return array An array of relation_ids.
	 */
	public function get_relation_ids_by_product_id( $product_id = 0, $relation_type = 'product' ) {

		if ( ! in_array( $relation_type, array( 'product', 'parent', 'both' ), true ) ) {
			return array();
		}

		global $wpdb;

		if ( 'product' === $relation_type ) {

			// phpcs:ignore
			return $wpdb->get_col( $wpdb->prepare( "SELECT relation_id FROM {$wpdb->prefix}wcqcs_quick_cross_sells WHERE product_id = %d ORDER BY product_order ASC, relation_id ASC;", $product_id ) );

		} elseif ( 'parent' === $relation_type ) {

			// phpcs:ignore
			return $wpdb->get_col( $wpdb->prepare( "SELECT relation_id FROM {$wpdb->prefix}wcqcs_quick_cross_sells WHERE parent_product_id = %d ORDER BY product_order ASC, relation_id ASC;", $product_id ) );

		} else {

			// phpcs:ignore
			return $wpdb->get_col( $wpdb->prepare( "SELECT relation_id FROM {$wpdb->prefix}wcqcs_quick_cross_sells WHERE ( parent_product_id = %d OR product_id = %d ) ORDER BY product_order ASC, relation_id ASC;", $product_id, $product_id ) );

		}
	}

}
