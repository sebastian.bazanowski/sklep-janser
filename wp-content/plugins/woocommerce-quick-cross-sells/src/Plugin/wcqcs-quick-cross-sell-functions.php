<?php
/**
 * Quick Cross-Sell WooCommerce Functions
 *
 * @package WPDesk\QuickCrossSells
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Format discount amount string to be vaild float
 *
 * @param string $amount Discount amount unformatted.
 *
 * @return string Discount amount formatted.
 */
function wcqcs_format_discount_amount( $amount = '' ) {

	$amount = (string) preg_replace( '/[^\d\,\.]/', '', $amount );
	$amount = (string) str_replace( ',', '.', $amount );

	$last_dot_pos = strrpos( $amount, '.' );
	if ( false !== $last_dot_pos ) {
		$amount = substr_replace( $amount, '{dec_point}', $last_dot_pos, 1 );
	}
	$amount = str_replace( array( '.', '{dec_point}' ), array( '', '.' ), $amount );

	$amount = wc_format_decimal( $amount, false, true );

	$amount = ( '0' === $amount ) ? '' : $amount;

	return $amount;
}

/**
 * Get Quick Cross-Sells section title
 *
 * @param int $product_id Product ID.
 *
 * @return string Product's Quick Cross-Sells section title.
 */
function wcqcs_get_product_section_title( $product_id = 0 ) {

	$title = get_option( 'wcqcs_section_default_title', wcqcs_get_default_wcq_section_title_value() );

	// Maybe get title from specific product.
	$product = wc_get_product( $product_id );
	if ( $product ) {
		$product_wcqcs_section_title = $product->get_meta( '_wcqcs_section_title', true );
		if ( $product_wcqcs_section_title ) {
			$title = $product_wcqcs_section_title;
		}
	}

	return apply_filters( 'wcqcs_section_title', $title, $product_id );
}

/**
 * Get plugin's default Quick Cross-Sells section title setting value
 *
 * @return string
 */
function wcqcs_get_default_wcq_section_title_value() {
	return __( 'You may also like', 'woocommerce-quick-cross-sells' );
}

/**
 * Get Quick Cross-Sell relation object by product_id and/or parent_product_id.
 *
 * @param int $product_id Product ID.
 * @param int $parent_product_id Product's parent ID.
 *
 * @return bool|\WPDesk\QuickCrossSells\QuickCrossSellRelation
 */
function wcqcs_get_quick_cross_sell_by_product_ids_pair( $product_id = 0, $parent_product_id = 0 ) {
	if ( ! $product_id ) {
		return false;
	}

	if ( ! $parent_product_id ) {
		$parent_product_id = get_the_ID();
	}

	$data_store = \WC_Data_Store::load( 'wcqcs-quick-cross-sell' );
	if ( ! is_a( $data_store, 'WC_Data_Store' ) || '\WPDesk\QuickCrossSells\QuickCrossSellDataStore' !== $data_store->get_current_class_name() ) {
		return false;
	}

	$relation_id = $data_store->get_relation_id_by_product_ids_pair( $parent_product_id, $product_id );

	if ( ! $relation_id ) {
		return false;
	}

	try {
		return new \WPDesk\QuickCrossSells\QuickCrossSellRelation( $relation_id );
	} catch ( Exception $e ) {
		return false;
	}

}

/**
 * Get Quick Cross-Sell relations by product_id
 *
 * @param int    $product_id Product ID.
 * @param string $relation_type Relation type. Possible values: 'product', 'parent', 'both'.
 * @param bool   $return_ids If return IDs only.
 *
 * @return bool|array
 */
function wcqcs_get_quick_cross_sells_by_product_id( $product_id = 0, $relation_type = 'product', $return_ids = false ) {

	$product_id = absint( $product_id );

	if ( ! $product_id ) {
		return false;
	}

	$data_store = \WC_Data_Store::load( 'wcqcs-quick-cross-sell' );
	if ( ! is_a( $data_store, 'WC_Data_Store' ) || '\WPDesk\QuickCrossSells\QuickCrossSellDataStore' !== $data_store->get_current_class_name() ) {
		return false;
	}

	$relation_ids = $data_store->get_relation_ids_by_product_id( $product_id, $relation_type );

	if ( $return_ids ) {

		// Return relation IDs only.
		return $relation_ids;

	} else {

		// Return relation QuickCrossSellRelation objects.
		$relations = array();
		if ( ! empty( $relation_ids ) ) {
			foreach ( $relation_ids as $relation_id ) {
				$relation = new \WPDesk\QuickCrossSells\QuickCrossSellRelation( $relation_id );
				if ( $relation->get_id() ) {
					$relations[] = $relation;
				}
			}
		}
		return $relations;

	}

}

/**
 * Delete Quick Cross-Sell relations by product_id
 *
 * @param int    $product_id Product ID.
 * @param string $relation_type Relation type. Possible values: 'product', 'parent', 'both'.
 * @param array  $exclude_relation_ids An array of relation IDs to be excluded.
 *
 * @return bool
 */
function wcqcs_delete_quick_cross_sells_by_product_id( $product_id = 0, $relation_type = 'product', $exclude_relation_ids = array() ) {

	$product_id = absint( $product_id );

	if ( ! $product_id ) {
		return false;
	}

	$exclude_relation_ids = array_filter( $exclude_relation_ids );
	$exclude_relation_ids = array_map( 'absint', $exclude_relation_ids );

	$data_store = \WC_Data_Store::load( 'wcqcs-quick-cross-sell' );
	if ( ! is_a( $data_store, 'WC_Data_Store' ) || '\WPDesk\QuickCrossSells\QuickCrossSellDataStore' !== $data_store->get_current_class_name() ) {
		return false;
	}

	$relations = $data_store->get_relation_ids_by_product_id( $product_id, $relation_type );
	if ( ! empty( $relations ) ) {
		foreach ( $relations as $relation_id ) {
			$relation_id = absint( $relation_id );
			if ( in_array( $relation_id, $exclude_relation_ids, true ) ) {
				continue;
			}

			$relation = new \WPDesk\QuickCrossSells\QuickCrossSellRelation( $relation_id );
			$data_store->delete( $relation );

		}
	}

	return true;

}

/**
 * Format product name string
 *
 * @param string $name Product name unformatted.
 *
 * @return string Product name formatted.
 */
function wcqcs_format_product_name( $name = '' ) {
	$name = trim( wp_strip_all_tags( (string) preg_replace( '/<span .*?class="(.*?description.*?)">(.*?)<\/span>/', ' $2', $name ) ) );

	return $name;
}

/**
 * Gets a list of product variation attributes for display on the frontend.
 *
 * @param \WC_Product $product A Product object.
 *
 * @return bool|array
 */
function wcqcs_get_formatted_product_variation_attributes( $product ) {

	if ( ! $product->get_id() ) {
		return false;
	}

	if ( ! in_array( $product->get_type(), array( 'variation', 'subscription_variation' ) ) ) {
		return false;
	}

	$product_attributes = $product->get_attributes();
	if ( ! is_array( $product_attributes ) ) {
		return false;
	}

	$attributes = array();

	// Variation values are shown only if they are not found in the title as of 3.0.
	// This is because variation titles display the attributes.
	foreach ( $product_attributes as $name => $value ) {
		$taxonomy = wc_attribute_taxonomy_name( str_replace( 'pa_', '', urldecode( $name ) ) );

		if ( taxonomy_exists( $taxonomy ) ) {
			// If this is a term slug, get the term's nice name.
			$term = get_term_by( 'slug', $value, $taxonomy, ARRAY_A );
			if ( false !== $term ) {
				$term = (array) $term;
				if ( isset( $term['name'] ) ) {
					$value = $term['name'];
				}
			}

			$label = wc_attribute_label( $taxonomy );
		} else {
			// If this is a custom option slug, get the options name.
			$value = apply_filters( 'woocommerce_variation_option_name', $value, null, $taxonomy, $product );
			$label = wc_attribute_label( str_replace( 'attribute_', '', $name ), $product );
		}

		// Check the nicename against the title.
		if ( '' === $value || wc_is_attribute_in_product_name( $value, $product->get_name() ) ) {
			continue;
		}

		$attributes[] = array(
			'key'   => $label,
			'value' => $value,
		);
	}

	if ( count( $attributes ) > 0 ) {
		return $attributes;
	}

	return false;
}

/**
 * Get product's calculated discount amount based on provided discounted price.
 *
 * @param int   $product_id Product ID.
 * @param float $discounted_price Product's discounted price.
 *
 * @return float|bool Product calculated discount amount or false if product doesn't exist.
 */
function wcqcs_get_product_calculated_discount_amount( $product_id = 0, $discounted_price = 0.00 ) {

	$product = wc_get_product( $product_id );
	if ( ! $product ) {
		return false;
	}

	$product_price    = (float) $product->get_price();
	$discounted_price = (float) $discounted_price;

	if ( $discounted_price >= $product_price ) {
		return 0;
	} elseif ( $discounted_price <= 0 ) {
		return $product_price;
	}

	$discount_amount = (float) $product_price - $discounted_price;
	$discount_amount = (float) wc_format_decimal( $discount_amount, '', true );

	return $discount_amount;
}
