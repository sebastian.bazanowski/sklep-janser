<?php 

/**
 * Plugin Name: Kokpit Kreacja
 * Description: Dodaje widgety Kreacji w Kokpicie
 * Version: 1.0
 * Author: StudioKreacja
 * Author URI: http://studiokreacja.pl/
 * Text Domain: kreacja
 * Network: true
 * License: GPL2
 */

class Kreacja_Dashboard_Widget {

	private $slug;
	private $title;
	private $link;
	private $img;


	public function __construct($slug,$title,$link,$img) 
	{
		$this->slug = 'kreacja_dashboard_'.$slug;
		$this->title = $title;
		$this->link = $link;
		$this->img = $img;

		add_action( 'wp_dashboard_setup', array($this,'register_callback') );

	}

	public function register_callback() 
	{
		wp_add_dashboard_widget(
                 $this->slug,         // Widget slug.
                 $this->title,         // Title.
                 array($this,'render') // Display function.
        );
	}

	public function render() {
		?>
		<style>
			#<?php echo $this->slug; ?> .inside {
				padding:0;
				margin-top:0;
			}
			#<?php echo $this->slug; ?> .inside img {
				display:block;
				max-width:100%;
			}
		</style>

		<?php if ($this->link): ?>
			<a href="<?php echo esc_attr($this->link) ?>" target="_blank" title="<?php echo esc_attr(__('Kliknij, aby zobaczyć więcej','kreacja')); ?>">
				<img src="<?php echo esc_attr($this->img); ?>" alt="<?php echo esc_attr($this->title); ?>">
			</a>			
		<?php else: ?>
			<img src="<?php echo esc_attr($this->img); ?>" alt="<?php echo esc_attr($this->title); ?>">
		<?php endif ?>

		<?php	
	}

	public static function fix_columns_width(){
		?>
		
		<style>
			@media only screen and (max-width: 1499px) and (min-width: 800px) {

				body #wpbody-content #dashboard-widgets .postbox-container,
				body #wpbody-content #dashboard-widgets #postbox-container-2, 
				body #wpbody-content #dashboard-widgets #postbox-container-3, 
				body #wpbody-content #dashboard-widgets #postbox-container-4 {
				width: 50%;
				}
			}
		</style>
		
		<?php
	}

}

$cms_widget = new Kreacja_Dashboard_Widget('cms','Jak korzystać z CMS?','http://www.studiokreacja.pl/poradnik/cms-lekcje-wideo/','//files.demooo.pl/wp_kokpit_obrazki/jak_korzystac_z_cms.png');
$pomoc_widget = new Kreacja_Dashboard_Widget('pomoc','Potrzebna dodatkowa pomoc?','http://www.studiokreacja.pl/kontakt/','//files.demooo.pl/wp_kokpit_obrazki/potrzebna_dodatkowa_pomoc.png');
$porady_widget = new Kreacja_Dashboard_Widget('porady','Porady marketingowo-biznesowe','http://www.studiokreacja.pl/blog/','//files.demooo.pl/wp_kokpit_obrazki/porady_marketingowo_biznesowe.png');
$kreacja_widget = new Kreacja_Dashboard_Widget('kreacja','Kreacja to nie tylko strony internetowe','http://www.studiokreacja.pl/oferta/','//files.demooo.pl/wp_kokpit_obrazki/oferta.png');

add_action('admin_head-index.php', array('Kreacja_Dashboard_Widget','fix_columns_width'));


