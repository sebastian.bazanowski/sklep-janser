import './sass/style.scss';
import './js/app.js';

import components from '../views/components/**/*.js';
import blocks from '../views/blocks/**/*.js';
import woocommerce from '../views/woocommerce/**/*.js';
