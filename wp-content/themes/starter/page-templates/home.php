<?php

/**
 * Template name: Strona główna
 */


$context = Timber\Timber::context();
$context['post'] = new Timber\Post();
$context['fields'] = get_fields();

  

Timber\Timber::render([
    '/page-templates/home.twig'
], $context, $_ENV['IS_TIMBER_CACHE_ENABLED_PERIOD'], Kreacja\Helpers\CacheHelper::getCacheMethod());
