<?php

/**
 * Template name: Wiedza
 */

global $paged;
if (!isset($paged) || !$paged){
  $paged = 1;
}

$context = Timber\Timber::context();
$context['post'] = new Timber\Post();
$context['categories'] = get_categories( array(
) );

$args = array(
  'posts_per_page' => 9,
  'paged' => $paged,
  'orderby' => array(
      'date' => 'DESC',
      
  ));
  
  $context['posts'] = new Timber\PostQuery($args);
  $context['current_term'] =  "knowledge";
  $context['pagination'] = $context['posts']->pagination(4);

Timber\Timber::render([
    'archive-' . $post->post_name . '.twig',
    'archive.twig'
], $context, $_ENV['IS_TIMBER_CACHE_ENABLED_PERIOD'], Kreacja\Helpers\CacheHelper::getCacheMethod());
