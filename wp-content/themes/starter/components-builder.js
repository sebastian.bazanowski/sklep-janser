const dree = require('dree');
const fs = require('fs');

const usedComponents = [/.git/, /menu\/horizontal/, /slider\/basic/];

const options = {
    depth: 3,
    exclude: usedComponents,
};

const removeFolder = (path) => {
    fs.rm(path, { recursive: true }, (err) => {
        if (err) {
            console.error(err);
        }
    });
};

const loopThrough = async (params, callback) => {
    await dree.scanAsync('./views/components', params).then(function (tree) {
        tree.children?.forEach((child) => {
            callback(child);
        });
    });
};

const makeCleanUp = async () => {
    await loopThrough(options, (child) => {
        child.children?.forEach((subchild) => {
            removeFolder(subchild.path);
        });
    });

    await loopThrough({ depth: 3 }, (child) => {
        if (!child.children) {
            removeFolder(child.path);
        }
    });
};

makeCleanUp();
