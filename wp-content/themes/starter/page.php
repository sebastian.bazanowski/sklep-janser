<?php

$context = Timber\Timber::context();
$context['post'] = new Timber\Post();

Timber\Timber::render([
    'page-' . $post->post_name . '.twig',
    'page.twig'
], $context, $_ENV['IS_TIMBER_CACHE_ENABLED_PERIOD'], Kreacja\Helpers\CacheHelper::getCacheMethod());
