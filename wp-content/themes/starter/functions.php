<?php
require __DIR__ . '/vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->safeLoad();

$timber = new Timber\Timber();

Timber\Timber::$cache = Kreacja\Helpers\CacheHelper::isActive();

require get_template_directory() . '/inc/app.php';

if (Kreacja\Helpers\WoocommerceHelper::isWoocommerceActive()) {
    require get_template_directory() . '/inc/Woocommerce/shop.php';
}

function add_style_select_buttons( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
  }

// Register our callback to the appropriate filter
add_filter( 'mce_buttons_2', 'add_style_select_buttons' );
//add custom styles to the WordPress editor
function my_custom_styles( $init_array ) {  

  $style_formats = array(  
    
      array(  
        'title' => 'Tekst 24px',  
        'block' => 'span',  
        'classes' => 'text-2xl',
        'wrapper' => false,
    ),
    array(  
      'title' => 'Tekst 30px',  
      'block' => 'span',  
      'classes' => 'text-3xl',
      'wrapper' => false,
  ),
  array(  
    'title' => 'Tekst 20px',  
    'block' => 'span',  
    'classes' => 'text-xl',
    'wrapper' => false,
),
array(  
    'title' => 'Tekst 14px',  
    'block' => 'span',  
    'classes' => 'text-xs',
    'wrapper' => false,
),
  );  

  

  // Insert the array, JSON ENCODED, into 'style_formats'
  $init_array['style_formats'] = json_encode( $style_formats );  

  return $init_array;  

} 
// Attach callback to 'tiny_mce_before_init' 
add_filter( 'tiny_mce_before_init', 'my_custom_styles' );

function theme_styles()  
{ 


	wp_enqueue_style( 'main', get_template_directory_uri() . '/assets/css/admin-style.css' );


}
add_action('wp_enqueue_scripts', 'theme_styles');
