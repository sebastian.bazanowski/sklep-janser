<?php

$context = Timber::context();
$context['post'] = new Timber\Post();

Timber::render([
    'single-' . $post->post_name . '.twig',
    'single.twig'
], $context, $_ENV['IS_TIMBER_CACHE_ENABLED_PERIOD'], Kreacja\Helpers\CacheHelper::getCacheMethod());
