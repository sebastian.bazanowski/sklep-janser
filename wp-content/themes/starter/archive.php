<?php

	global $paged;
	if (!isset($paged) || !$paged){
		$paged = 1;
	}


$context = Timber\Timber::context();
$context['archives'] = new TimberArchives();
$context['current_term'] =  $taxonomy = new \Timber\Term( get_queried_object() );
$context['categories'] = get_categories( array(
    'orderby' => 'date',
    'order'   => 'DESC'
) );


$context['pagination'] = $context['posts']->pagination(4);
Timber\Timber::render([
    'archive-' . $post->post_name . '.twig',
    'archive.twig'
], $context, $_ENV['IS_TIMBER_CACHE_ENABLED_PERIOD'], Kreacja\Helpers\CacheHelper::getCacheMethod());
