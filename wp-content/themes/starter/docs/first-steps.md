# First steps

Once you have cloned the repo, do the following thigs:

### 1. Clone the components repo:

The components directory is a separate repo on GitLab, used as a submodule. In order to clone the repo, use the following commands in the terminal:

```shell
git submodule init
git submodule update
```

(If these commands don't work, check if there's a .gitmodule file in the parent directory.)

This way you will get the components directory inside `/wp-content/themes/starter/views`. If you don't clone the components repo, you will probably see a lot of errors when you visit the page. If you want to learn more about the components, visit the [components](components.md) page.

### 2. Remove the .git directory

Each project should be kept in a separate repo. That is why the next thing you have to do is to remove the .git directory from the parent directory, create a new repo on GitLab and push the project to the new repo. This way the changes you make in the project files will not affect the starter theme.

You should also remove the .git directory from `/wp-content/themes/starter/views/components`. If you don't do that, the changes you will make in this directory will be pushed to the components repo and we don't want that.

### 3. Install packages and start the development server

```shell
npm install
npm run dev
```
