# Twig

The instance of the Twig class is created in `inc/app.php`

## Custom Twig functions and filters

Twig has a couple of built-in functions and filters, but we created an API to be able to easily make ours.

### Creating custom functions

To create a custom function, go to `inc/Twig/Functions` and create a new PHP class. That's all, the rest is done for you.

`inc/Twig/Functions/Awesome_Function.php`

```php
namespace Kreacja\Twig\Functions;

class Awesome_Function
{
    public function init($someParams)
    {
        // Code goes here.
    }
}

```

```twig
{{ awesome_function(someParams) }}
```

### Creating custom filters

The same way you create new Twig functions, you can easily add more filters.

`inc/Twig/Filters/Awesome_Filter.php`

```php
namespace Kreacja\Twig\Filters;

class Awesome_Filter
{
    public function init($someParams)
    {
        return // Code goes here.
    }
}

```

```twig
{{ awesome_filter(someParams) }}
```

### IMPORTANT!

If you add a new function/filter but it doesn't work, probably you have to run one command in the terminal:

```shell
composer dump-autoload
```
