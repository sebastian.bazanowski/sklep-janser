# Configuration

#### The .env file

The .env file is used to set some options important for the proper functioning of the theme. There's a comment above each line explaining what a given entry does and how you should use it. 

When you start working on a new project, the only thing you'll probably need to change is the line related to WooCommerce.

Once the project is ready and deployed to the production server, come back to this file to change the mode and enable caching.

```
# Once the site is ready, set IS_TIMBER_CACHE_ENABLED to true
IS_TIMBER_CACHE_ENABLED=false
# 604800 = 7 days
IS_TIMBER_CACHE_ENABLED_PERIOD=604800
# Once the site is ready, set IS_TIMBER_CACHE_ENABLED_METHOD to Timber\Loader::CACHE_TRANSIENT instead of Timber\Loader::CACHE_NONE
IS_TIMBER_CACHE_ENABLED_METHOD=Timber\Loader::CACHE_NONE
# WooCommerce Support
IS_WOOCOMMERCE_ENABLED=true
# Once the site is ready, set MODE to production
MODE=development

```

#### /inc/Core/Theme.php file

If you're using a webfont in your project, you'll have to update the link presented below.

```php
$font_url = str_replace(',', '%2C', '//fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap');
```
---

By default, the theme has four menus registered. 

```php
register_nav_menus(
    [
        'primary' => __('Menu główne', 'starter'),
        'footer' => __('Menu w stopce', 'starter'),
        'footer_two' => __('Menu w stopce 2', 'starter'),
        'footer_three' => __('Menu w stopce ', 'starter')
    ]
);
```

#### /views/partials/header.twig file

If you need to import third-party styles or scripts, you can do it in the `<head>` section of the page.

```twig
<head>
    <meta charset="{{ site.charset }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    {{ function("wp_head") }}

    {% set time = function('time') %}
    <link rel="stylesheet" href="{{ theme.link }}/dist/css/main.css?ver={{time}}">
    <link rel="stylesheet" media="print" onload="this.media='all'" href="{{ theme.link }}/style.css">

    {{ options.head_scripts }}
</head>
```
