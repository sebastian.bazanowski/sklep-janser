# Advanced Custom Fields

The instantion of CustomFields class is called in inc/app.php

## Blocks

To create a Gutenberg Block by ACF go to incs/Blocks folder.\
Then create a class which implement Interface BlocksInterface.

| Method         |  Type  |                            Description |
| -------------- | :----: | -------------------------------------: |
| init           | method |        Initialize the gutenberg block. |
| renderCallback | method | Render the block view with passed Data |

```php

// inc/Blocks/Button/Button.php

namespace Kreacja\Blocks\Button;

use Kreacja\Interfaces\BlocksInterface;
use Timber\Timber;

class Button implements BlocksInterface
{
    public static function init()
    {
        if (function_exists('acf_register_block_type')) {

            // register a testimonial block.
            acf_register_block_type(array(
                'name'              => 'button',
                'title'             => __('Przycisk'),
                'description'       => __('Blok służący do umieszczenia przycisku z wybranym tekstem i linkiem'),
                'render_callback'   => [Button::class, 'renderCallback'],
                'category'          => 'formatting',
                'icon'              => 'admin-comments',
                'keywords'          => array('button', 'przycisk'),
            ));
        }
    }

    public function renderCallback($block, $content = '', $is_preview = false)
    {
        $context = Timber::context();

        // Store block values.
        $context['block'] = $block;

        // Store field values.
        $context['fields'] = get_fields();

        // Store $is_preview value.
        $context['is_preview'] = $is_preview;

        // Render the block.
        Timber::render('views/blocks/Button/index.twig', $context);
    }
}

```

To register block, just pass the class into addBlock method.

```php

// inc/app.php
use Kreacja\Blocks\Button\Button;

$customFields->addBlock(Button::class);

```

## Options

### Option Subpage

To create subpage, simply call OptionSubpage class in inc/app.php file.

```php
// inc/app.php
$theme_settings_option = new OptionSubpage('Ustawienia motywu', 'Ustawienia motywu', 'themes.php');
$customFields->addOptionSubpage($theme_settings_option);
```

### Option page

To create page, simply call OptionPage class in inc/app.php file.

```php
// inc/app.php
$woocommerce_settings_option = new OptionPage('Ustawienia sklepu', 'Ustawienia Sklepu', 'shop-settings-general', 'dashicons-money-alt');
$customFields->addOptionPage($woocommerce_settings_option);
```
