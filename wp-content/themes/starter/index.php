<?php

$context = Timber::context();
$context['posts'] = new Timber\PostQuery();

Timber::render([
    'archive-' . $post->post_name . '.twig',
    'index.twig'
], $context, $_ENV['IS_TIMBER_CACHE_ENABLED_PERIOD'], Kreacja\Helpers\CacheHelper::getCacheMethod());
