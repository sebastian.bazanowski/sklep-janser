module.exports = {
    content: [
        './assets/**/*.{scss,js}',
        './views/**/*.twig',
        './woocommerce/**/*.php',
        './inc/**/*.php',
    ],
    theme: {
        screens: {
            xs: '360px',
            sm: '426px',
            md: '768px',
            lg: '1024px',
            xl: '1168px',
            xxl: '1280px',
            xxxl: '1544px',
        },
        fontFamily: {
            sans: ['Poppins', 'sans-serif'],
            serif: ['serif'],
        },
        extend: {
            colors: {
                primaryBlue: '#0068B6',
                secondaryGreen: '#00993B',
                light: '#F5F7FA',
                medium: '#DCDEE0',                     
                dark: '#16273D',
                primary: '#0F1E29',
                secondary: '#20313D',
                tertiary: '#565F66',
                blueOpacity:'#DFE8F4',
                greenOpacity:'#00973b29',
                catskill:'#F5F7FA',

                lightgrayBorder:'#DDDEE0',                  
                grayBorder: '#d8d8d8',
            },
        },
    },
    plugins: [require('@tailwindcss/forms')],
};
