<?php

namespace Kreacja\Modules\Search;

use Kreacja\Helpers\WoocommerceHelper;

final class Search
{
    public function __construct()
    {
        add_action('rest_api_init', [$this, 'registerRoutes']);
    }

    private function getSearchTypes()
    {
        if (WoocommerceHelper::isWoocommerceActive()) {
            return ["Categories", "Products"];
        } else {
            return  ["Pages", "Posts"];
        }
    }


    public function registerRoutes()
    {
        register_rest_route('kreacja/v1', 'search', [
            'methods' => 'GET',
            'callback' => [$this, 'buildAPI'],
            'callback_permission' => '_return_true',
        ]);
    }

    public function buildAPI($data)
    {
        $results = [];

        foreach ($this->getSearchTypes() as $type) {
            $namespaced_class = __NAMESPACE__ . '\\SearchTypes\\' . $type;
            $namespaced_class = new $namespaced_class();
            $results[] = $namespaced_class->getData($data);
        }

        return $results;
    }
}
