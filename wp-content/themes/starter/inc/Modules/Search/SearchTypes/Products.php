<?php

namespace Kreacja\Modules\Search\SearchTypes;

use Kreacja\Interfaces\SearchInterface;

class Products implements SearchInterface
{
    private $results = [
        "heading" => "Produkty",
        "data" => []
    ];

    public function getData($data)
    {
        $productsFromSKU = $this->getProductsBySKU($data);
        $productsFromName = $this->getProductsByName($data);

        $products = array_merge($productsFromSKU, $productsFromName);

        $products = $this->unique($products);
        $this->results["data"] = $products;

        return $this->results;
    }

    public function getProductsByName($data)
    {
        $products = wc_get_products(array(
            'status' => 'publish',
            's' => sanitize_text_field($data['key']),
            'limit' => 12,
        ));

        $results = [];

        foreach ($products as $product) {
            $results[] = $this->getProductData($product);
        };

        return $results;
    }

    public function getProductsBySKU($data)
    {
        $args = [
            'post_type'         =>  'product',
            'posts_per_page'    => -1,
            'meta_query' => [
                [
                    'key' => '_sku',
                    'value' => $data['key'],
                    'compare' => 'LIKE'
                ]
            ]
        ];

        $query = new \WP_Query($args);

        $results = [];

        while ($query->have_posts()) {
            $query->the_post();
            $product = wc_get_product(get_the_ID());

            $results[] = $this->getProductData($product);
        };

        return $results;
    }

    private function getProductData($product)
    {
        $imageUrl = wp_get_attachment_image_src(get_post_thumbnail_id($product->id));
        return [
            'id'        =>  $product->get_id(),
            'name'      => $product->name,
            'permalink' => get_permalink($product->id),
            'images'    => [
                'thumbnail' => $imageUrl[0],
            ],
            'price'     => wc_price(wc_get_price_to_display($product, array('price' => $product->get_regular_price()))),
            'sku'       => $product->get_sku()
        ];
    }

    private function unique($data = [], $column = "id")
    {
        $usedIds = [];

        $results = array_map(function ($single) use ($column, &$usedIds) {
            if (!in_array($single[$column], $usedIds)) {
                $usedIds[] = $single[$column];
                return $single;
            }
        }, $data);

        return array_filter($results);
    }
}
