<?php

namespace Kreacja\Modules\Search\SearchTypes;

use Kreacja\Interfaces\SearchInterface;

class Categories implements SearchInterface
{
    private $results = [
        "heading" => "Kategorie",
        "data" => []
    ];

    public function getData($data)
    {
        $terms = get_terms([
            'search' => $data['key'],
            'taxonomy' => 'product_cat'
        ]);


        foreach ($terms as $term) {
            $this->results['data'][] = [
                'name' => $term->name,
                'permalink' => get_term_link($term)
            ];
        }
        return $this->results;
    }
}
