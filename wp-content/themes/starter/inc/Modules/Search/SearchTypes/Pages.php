<?php

namespace Kreacja\Modules\Search\SearchTypes;

use Kreacja\Interfaces\SearchInterface;

class Pages implements SearchInterface
{
    private $results = [
        "heading" => "Strony",
        "data" => []
    ];

    public function getData($data)
    {
        $args = [
            'post_type' => 'page',
            's' => sanitize_text_field($data['key']),
        ];

        $q = new \WP_Query($args);


        if ($q->have_posts()) :
            while ($q->have_posts()) : $q->the_post();
                $imageUrl = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_id()));
                $this->results['data'][] = [
                    'id'        => get_the_id(),
                    'name'      => get_the_title(),
                    'permalink' => get_permalink(get_the_id()),
                ];
            endwhile;
        endif;

        wp_reset_postdata();

        return $this->results;
    }
}
