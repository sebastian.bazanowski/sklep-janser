<?php

    namespace Kreacja\Utils;

    class KeyFromStringUtil
    {
        public static function make(string $text)
        {
            $prepared_key = strtolower($text);
            $prepared_key = trim($prepared_key);
            $prepared_key = str_replace(' ', '', $prepared_key);
    
            return $prepared_key;
        }
    }