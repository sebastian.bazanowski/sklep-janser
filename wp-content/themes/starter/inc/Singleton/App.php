<?php

namespace Kreacja\Singleton;

use Kreacja\Acf\CustomFields;
use Kreacja\Twig\Twig;
use Kreacja\Core\Theme;
use Kreacja\Singleton\Singleton;

final class App extends Singleton
{

    public $twig = null;

    public $acf = null;

    public $theme = null;

    protected function __construct()
    {
        $this->twig = new Twig();
        $this->acf = new CustomFields();
        $this->theme = new Theme();
    }
}
