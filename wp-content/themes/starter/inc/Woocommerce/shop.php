<?php

namespace Kreacja\Woocommerce;

use Kreacja\Woocommerce\Badges\SaleBadge;
use Kreacja\Woocommerce\Badges\DeliverTimeBadge;
use Kreacja\Woocommerce\Badges\FlexibleCategoryBadge;
use Kreacja\Woocommerce\Badges\ReturnTimeBadge;
use Kreacja\Acf\OptionSubpage;
use Kreacja\Acf\OptionPage;
use Kreacja\Blocks\ProductsSlider\ProductsSlider;
use Kreacja\Core\Taxonomies\HighlightedCategories;
use Kreacja\Factories\TaxonomyFactory;
use Kreacja\Factories\AjaxFactory;
use Kreacja\Singleton\App;
use Kreacja\Core\Ajax\AjaxAddToCart;
use Kreacja\Core\Ajax\AjaxGetLatestProductATC;
use Kreacja\Core\Ajax\AjaxRemoveCartItem;
use Kreacja\Core\Ajax\AjaxGetCart;

$app = App::getInstance();

/**
 *  Initialize Badges 
 */

$saleBadge = new SaleBadge(15);
$deliveryTimeBadge = new DeliverTimeBadge(15);
$returnTimeBadge = new ReturnTimeBadge(15);
$flexibleCategoryBadge = new FlexibleCategoryBadge(15);

/**
 *  Initialize Shop Option Pages 
 */

$app->acf->addOptionPage(
    new OptionPage('Ustawienia sklepu', 'Ustawienia Sklepu', 'shop-settings-general', 'dashicons-money-alt')
);

$app->acf->addOptionSubpage(
    new OptionSubpage('Globalne zakładki', 'Globalne zakładki', 'edit.php?post_type=product')
);

/**
 *  Initialize Shop Blocks
 */

$app->acf->addBlock(ProductsSlider::class);

/**
 *  Initialize Shop Taxonomies
 */

TaxonomyFactory::create(HighlightedCategories::class);


add_filter("timber/context", function ($context) {

    $context['cart'] = WC()->cart;

    return $context;
});

AjaxFactory::create(AjaxAddToCart::class, 'kreacja_add_to_cart');
AjaxFactory::create(AjaxGetLatestProductATC::class, 'kreacja_ajax_latest_atc');
AjaxFactory::create(AjaxRemoveCartItem::class, 'remove_item_from_cart');
AjaxFactory::create(AjaxGetCart::class, 'kreacja_get_cart_items');
