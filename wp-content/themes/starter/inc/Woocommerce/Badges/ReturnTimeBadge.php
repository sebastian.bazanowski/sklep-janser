<?php

namespace Kreacja\Woocommerce\Badges;

use Kreacja\Woocommerce\Badges\Badge;


class ReturnTimeBadge extends Badge
{

    public function init($product_id)
    {
        $product_return_time = get_field('single_product_return_time', $product_id);
        $global_return_time = get_field('return_product_time', 'options');

        if ($product_return_time && ($product_return_time > $global_return_time)) {
            echo "<span class='Badge Badge--return-time'>" . __('Wydłużony czas na zwrot', 'starter') . "</span>";
        }
    }
}
