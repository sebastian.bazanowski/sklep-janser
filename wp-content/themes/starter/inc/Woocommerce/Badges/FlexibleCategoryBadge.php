<?php

namespace Kreacja\Woocommerce\Badges;

use Kreacja\Woocommerce\Badges\Badge;


class FlexibleCategoryBadge extends Badge
{

    public function init($product_id)
    {

        $highlighted = get_the_terms($product_id, [
            'taxonomy'  =>  'wyroznione',
        ]);

        if (empty($highlighted)) return;

        foreach ($highlighted as $highlight) {
            $bg_color = get_field('badge_bg_color', $highlight);
            $font_color = get_field('font_color', $highlight);
            $shortname = get_field('short_name', $highlight);

            if ($shortname === "") {
                $shortname = $highlight->name;
            }

            echo "<span class='Badge Badge--flexible' style='background-color:" . $bg_color . "; color:" . $font_color . ";'>{$shortname}</span>";
        }
    }
}

