<?php

namespace Kreacja\Woocommerce\Badges;

use Kreacja\Woocommerce\Badges\Badge;


class DeliverTimeBadge extends Badge
{

    public function init($product_id)
    {
        $product_delivery_time = get_field('single_product_delivery_time', $product_id);
        $global_delivery_time = get_field('shop_delivery_time', 'options');

        if ($product_delivery_time && ($product_delivery_time < $global_delivery_time)) {
            echo "<span class='Badge Badge--delivery-time'>" . __('Szybka dostawa', 'starter') . "</span>";
        }
    }
}
