<?php

namespace Kreacja\Woocommerce\Badges;

use Kreacja\Woocommerce\Badges\Badge;


class SaleBadge extends Badge
{

    private $discount = 0;


    private function countPercentage( $sale_price = 0, $regular_price = 0 )
    {
        return round(100 - (( $sale_price / $regular_price ) * 100), 0);
    }

    public function init($product_id)
    {
        $product = wc_get_product($product_id);

        if (!$product) return;

        if (!$product->is_type('simple') || !$product->is_on_sale()) return;

        $calculated_discount = round(100 - (($product->get_sale_price() / $product->get_regular_price()) * 100), 0);

        $to_formated = "";
        if ($product->get_date_on_sale_to()) {
            $to = new \DateTime($product->get_date_on_sale_to());
            $to_formated = sprintf(__('<em>(do %s)</em>', 'starter'), $to->format('d.m.Y'));
        }

        if ( $product->is_type('simple') ) 
        {
            $this->discount = $this->countPercentage( $product->get_sale_price(), $product->get_regular_price() );
        } 
        elseif ( $product->is_type('variable') ) 
        {
            $variations = $product->get_available_variations();
            $variations_sales = array_map( function( $variation ) {
                return $this->countPercentage( $variation['display_price'], $variation['display_regular_price'] );
            }, $variations);

            $this->discount = max($variations_sales);
        }

        echo "<span class='Badge Badge--onsale onsale'>-{$this->discount}% {$to_formated}</span>";
    }
}
