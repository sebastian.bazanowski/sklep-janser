<?php

namespace Kreacja\Woocommerce\Badges;

abstract class Badge
{

    private $priority = 10;

    public function __construct($priority)
    {
        $this->priority = $priority;

        add_action('woocommerce_show_loop_item_badges', [$this, 'init'], $priority, 2);
    }

    abstract public function init($product_id);
}
