<?php

namespace Kreacja\Acf;

class OptionSubpage
{
    private $page_title = "";

    private $menu_title = "";

    private $parent_slug = "";

    public function __construct($page_title = "", $menu_title = "", $parent_slug = "")
    {
        $this->page_title = $page_title;
        $this->menu_title = $menu_title;
        $this->parent_slug = $parent_slug;

        $this->add($this->page_title, $this->menu_title, $this->parent_slug);
    }

    public function add($page_title, $menu_title, $parent_slug)
    {
        if (!function_exists('acf_add_options_page')) {
            throw new Exception('Function acf_add_options_sub_page doesnt exist.');
        }

        acf_add_options_sub_page([
            'page_title'    => $page_title,
            'menu_title'    => $menu_title,
            'parent_slug'   => $parent_slug,
        ]);
    }
}
