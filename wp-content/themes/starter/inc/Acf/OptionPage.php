<?php

namespace Kreacja\Acf;

class OptionPage
{
    private $page_title = "";

    private $menu_title = "";

    private $menu_slug = "";

    private $menu_icon = "";

    public function __construct($page_title = "", $menu_title = "", $menu_slug = "", $menu_icon = "")
    {
        $this->page_title = $page_title;
        $this->menu_title = $menu_title;
        $this->menu_slug = $menu_slug;
        $this->menu_icon = $menu_icon;

        $this->add();
    }

    public function add()
    {
        if (!function_exists('acf_add_options_page')) {
            throw new Exception('Function acf_add_options_sub_page doesnt exist.');
        }

        acf_add_options_page([
            'page_title'    => $this->page_title,
            'menu_title'    => $this->menu_title,
            'menu_slug'     => $this->menu_slug,
            'icon_url'      => $this->menu_icon,
            'capability'    =>  'edit_posts'
        ]);
    }
}
