<?php

namespace Kreacja\Acf;

class CustomFields
{

    private $subpages = [];

    private $pages = [];

    public function __construct()
    {
        $this->loadOptionSubpages();
        $this->loadOptionPages();
    }

    public function addOptionSubpage($subpage)
    {
        $this->subpages[] = $subpage;
    }

    public function addOptionPage($page)
    {
        $this->pages[] = $page;
    }

    public function loadOptionSubpages()
    {
        add_action('acf/init', function () {
            return $this->subpages;
        });
    }

    public function loadOptionPages()
    {
        add_action('acf/init', function () {
            return $this->pages;
        });
    }

    public function addBlock($class)
    {
        add_action('acf/init', [$class, 'init']);
    }
}
