<?php

namespace Kreacja\Blocks\Arguments;

use Kreacja\Interfaces\BlocksInterface;
use Timber\Timber;

class Arguments implements BlocksInterface
{
    public static function init()
    {
        if (function_exists('acf_register_block_type')) {

            // register a testimonial block.
            acf_register_block_type(array(
                'name'              => 'Arguments',
                'title'             => __('Dlaczego warto?'),
                'description'       => __('Blok służący do wyświetlenia sekcji Dlaczego warto'),
                'render_callback'   => [Arguments::class, 'renderCallback'],
                'category'          => 'formatting',
                'icon'              => 'admin-comments',
                'keywords'          => array('Arguments', 'Dlaczego warto?'),
            ));
        }
    }

    public static function renderCallback($block, $content = '', $is_preview = false)
    {
        $context = Timber::context();

        // Store block values.
        $context['block'] = $block;

        // Store field values.
        $context['fields'] = get_fields();
        $context['benefits'] = get_field('benefits', 55);

        // Store $is_preview value.
        $context['is_preview'] = $is_preview;

        // Render the block.
        Timber::render('views/blocks/Arguments/index.twig', $context);
    }
}
