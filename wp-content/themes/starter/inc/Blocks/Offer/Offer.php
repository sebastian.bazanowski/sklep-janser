<?php

namespace Kreacja\Blocks\Offer;

use Kreacja\Interfaces\BlocksInterface;
use Timber\Timber;

class Offer implements BlocksInterface
{
    public static function init()
    {
        if (function_exists('acf_register_block_type')) {

            // register a testimonial block.
            acf_register_block_type(array(
                'name'              => 'Offer',
                'title'             => __('Oferta'),
                'description'       => __('Blok służący do wyświetlenia sekcji Oferta'),
                'render_callback'   => [Offer::class, 'renderCallback'],
                'category'          => 'formatting',
                'icon'              => 'admin-comments',
                'keywords'          => array('Offer', 'Oferta'),
            ));
        }
    }

    public static function renderCallback($block, $content = '', $is_preview = false)
    {
        $context = Timber::context();

        // Store block values.
        $context['block'] = $block;

        // Store field values.
        $context['fields'] = get_fields();

        // Store $is_preview value.
        $context['is_preview'] = $is_preview;

        // Render the block.
        Timber::render('views/blocks/Offer/index.twig', $context);
    }
}
