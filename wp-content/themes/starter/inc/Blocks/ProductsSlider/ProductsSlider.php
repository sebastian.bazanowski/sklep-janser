<?php

namespace Kreacja\Blocks\ProductsSlider;

use Kreacja\Interfaces\BlocksInterface;
use Timber\Timber;

class ProductsSlider implements BlocksInterface
{
    public static function init()
    {
        if (function_exists('acf_register_block_type')) {

            // register a testimonial block.
            acf_register_block_type(array(
                'name'              => 'products-slider',
                'title'             => __('Slider produktów'),
                'description'       => __('Blok służący do wyświetlania wybranych produktów'),
                'render_callback'   => [ProductsSlider::class, 'renderCallback'],
                'category'          => 'formatting',
                'icon'              => 'admin-comments',
                'keywords'          => array('slider', 'produkty'),
            ));
        }
    }

    public static function renderCallback($block, $content = '', $is_preview = false)
    {
        $context = Timber::context();

        $context['block'] = $block;
        $context['fields'] = get_fields();
        $context['is_preview'] = $is_preview;

        if ($context['fields']['what-to-display'] === 'categories') {
            $context['selected_products'] = Timber::get_posts([
                'post_type' => 'product',
                'tax_query' => [
                    [
                        'taxonomy' => 'product_cat',
                        'terms' => $context['fields']['selected_category']
                    ]
                ]
            ]);
        } else {
            $ids = array_column($context['fields']['selected_products'], 'ID');
            
            $context['selected_products'] = Timber::get_posts([
                'post_type' => 'product',
                'post__in' => $ids
            ]);
        }


        // Render the block.
        Timber::render('views/blocks/ProductsSlider/index.twig', $context);
    }
}
