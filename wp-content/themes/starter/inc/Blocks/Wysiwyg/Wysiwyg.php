<?php

namespace Kreacja\Blocks\Wysiwyg;

use Kreacja\Interfaces\BlocksInterface;
use Timber\Timber;

class Wysiwyg implements BlocksInterface
{
    public static function init()
    {
        if (function_exists('acf_register_block_type')) {

            // register a testimonial block.
            acf_register_block_type(array(
                'name'              => 'Wysiwyg',
                'title'             => __('Wysiwyg'),
                'description'       => __('Blok służący do wyświetlenia Wysiwyg'),
                'render_callback'   => [Wysiwyg::class, 'renderCallback'],
                'category'          => 'formatting',
                'icon'              => 'admin-comments',
                'keywords'          => array('Wysiwyg', 'text', 'edytor'),
            ));
        }
    }

    public static function renderCallback($block, $content = '', $is_preview = false)
    {
        $context = Timber::context();

        // Store block values.
        $context['block'] = $block;

        // Store field values.
        $context['fields'] = get_fields();

        // Store $is_preview value.
        $context['is_preview'] = $is_preview;

        // Render the block.
        Timber::render('views/blocks/Wysiwyg/index.twig', $context);
    }
}
