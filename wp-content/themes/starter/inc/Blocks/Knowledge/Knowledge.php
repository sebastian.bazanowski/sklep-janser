<?php

namespace Kreacja\Blocks\Knowledge;

use Kreacja\Interfaces\BlocksInterface;
use Timber\Timber;

class Knowledge implements BlocksInterface
{
    public static function init()
    {
        if (function_exists('acf_register_block_type')) {

            // register a testimonial block.
            acf_register_block_type(array(
                'name'              => 'Knowledge',
                'title'             => __('Wiedza'),
                'description'       => __('Blok służący do wyświetlenia sekcji Wiedza'),
                'render_callback'   => [Knowledge::class, 'renderCallback'],
                'category'          => 'formatting',
                'icon'              => 'admin-comments',
                'keywords'          => array('Wiedza', 'Knowledge'),
            ));
        }
    }

    public static function renderCallback($block, $content = '', $is_preview = false)
    {
        $context = Timber::context();

        // Store block values.
        $context['block'] = $block;

        // Store field values.
        $context['fields'] = get_fields();

        $context['categories'] = get_categories( array(
            ) );

        // Store $is_preview value.
        $context['is_preview'] = $is_preview;

        // Render the block.
        Timber::render('views/blocks/Knowledge/index.twig', $context);
    }
}
