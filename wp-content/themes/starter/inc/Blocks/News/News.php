<?php

namespace Kreacja\Blocks\News;

use Kreacja\Interfaces\BlocksInterface;
use Timber\Timber;

class News implements BlocksInterface
{
    public static function init()
    {
        if (function_exists('acf_register_block_type')) {

            // register a testimonial block.
            acf_register_block_type(array(
                'name'              => 'News',
                'title'             => __('Aktualności'),
                'description'       => __('Blok służący do wyświetlenia sekcji Aktualności'),
                'render_callback'   => [News::class, 'renderCallback'],
                'category'          => 'formatting',
                'icon'              => 'admin-comments',
                'keywords'          => array('News', 'Aktualności'),
            ));
        }
    }

    public static function renderCallback($block, $content = '', $is_preview = false)
    {
        $context = Timber::context();

        // Store block values.
        $context['block'] = $block;

        // Store field values.
        $context['fields'] = get_fields();

        $args = array(
            'post_type' => 'post',
            'posts_per_page' => 3,
            'cat' => 27,
            'orderby' => array(
                'date' => 'DESC',
            ));
            
            $context['blog'] = Timber::get_posts( $args );

        // Store $is_preview value.
        $context['is_preview'] = $is_preview;

        // Render the block.
        Timber::render('views/blocks/News/index.twig', $context);
    }
}
