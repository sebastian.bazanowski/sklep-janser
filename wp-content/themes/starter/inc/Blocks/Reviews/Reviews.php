<?php

namespace Kreacja\Blocks\Reviews;

use Kreacja\Interfaces\BlocksInterface;
use Timber\Timber;

class Reviews implements BlocksInterface
{
    public static function init()
    {
        if (function_exists('acf_register_block_type')) {

            // register a testimonial block.
            acf_register_block_type(array(
                'name'              => 'Reviews',
                'title'             => __('Opinie i Social media'),
                'description'       => __('Blok służący do wyświetlenia sekcji Opinie i Social media'),
                'render_callback'   => [Reviews::class, 'renderCallback'],
                'category'          => 'formatting',
                'icon'              => 'admin-comments',
                'keywords'          => array('Reviews', 'Reviews'),
            ));
        }
    }

    public static function renderCallback($block, $content = '', $is_preview = false)
    {
        $context = Timber::context();

        // Store block values.
        $context['block'] = $block;

        // Store field values.
        $context['fields'] = get_fields();

        // Store $is_preview value.
        $context['is_preview'] = $is_preview;

        // Render the block.
        Timber::render('views/blocks/Reviews/index.twig', $context);
    }
}
