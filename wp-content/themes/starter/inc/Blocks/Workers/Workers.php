<?php

namespace Kreacja\Blocks\Workers;

use Kreacja\Interfaces\BlocksInterface;
use Timber\Timber;

class Workers implements BlocksInterface
{
    public static function init()
    {
        if (function_exists('acf_register_block_type')) {

            // register a testimonial block.
            acf_register_block_type(array(
                'name'              => 'Workers',
                'title'             => __('Pracownicy'),
                'description'       => __('Blok służący do wyświetlenia sekcji Pracownicy'),
                'render_callback'   => [Workers::class, 'renderCallback'],
                'category'          => 'formatting',
                'icon'              => 'admin-comments',
                'keywords'          => array('Workers', 'Pracownicy', 'Lista osób'),
            ));
        }
    }

    public static function renderCallback($block, $content = '', $is_preview = false)
    {
        $context = Timber::context();

        // Store block values.
        $context['block'] = $block;

        // Store field values.
        $context['fields'] = get_fields();

        // Store $is_preview value.
        $context['is_preview'] = $is_preview;

        // Render the block.
        Timber::render('views/blocks/Workers/index.twig', $context);
    }
}
