<?php

namespace Kreacja\Blocks\Button;

use Kreacja\Interfaces\BlocksInterface;
use Timber\Timber;

class Button implements BlocksInterface
{
    public static function init()
    {
        if (function_exists('acf_register_block_type')) {

            // register a testimonial block.
            acf_register_block_type(array(
                'name'              => 'button',
                'title'             => __('Przycisk'),
                'description'       => __('Blok służący do umieszczenia przycisku z wybranym tekstem i linkiem'),
                'render_callback'   => [Button::class, 'renderCallback'],
                'category'          => 'formatting',
                'icon'              => 'admin-comments',
                'keywords'          => array('button', 'przycisk'),
            ));
        }
    }

    public static function renderCallback($block, $content = '', $is_preview = false)
    {
        $context = Timber::context();

        // Store block values.
        $context['block'] = $block;

        // Store field values.
        $context['fields'] = get_fields();

        // Store $is_preview value.
        $context['is_preview'] = $is_preview;

        // Render the block.
        Timber::render('views/blocks/Button/index.twig', $context);
    }
}
