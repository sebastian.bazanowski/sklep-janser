<?php

namespace Kreacja\Blocks\Newsletter;

use Kreacja\Interfaces\BlocksInterface;
use Timber\Timber;

class Newsletter implements BlocksInterface
{
    public static function init()
    {
        if (function_exists('acf_register_block_type')) {

            // register a testimonial block.
            acf_register_block_type(array(
                'name'              => 'Newsletter',
                'title'             => __('Newsletter'),
                'description'       => __('Blok służący do wyświetlenia sekcji Newsletter'),
                'render_callback'   => [Newsletter::class, 'renderCallback'],
                'category'          => 'formatting',
                'icon'              => 'admin-comments',
                'keywords'          => array('Newsletter', 'Newsletter'),
            ));
        }
    }

    public static function renderCallback($block, $content = '', $is_preview = false)
    {
        $context = Timber::context();

        // Store block values.
        $context['block'] = $block;

        // Store field values.
        $context['fields'] = get_fields();

        // Store $is_preview value.
        $context['is_preview'] = $is_preview;

        // Render the block.
        Timber::render('views/blocks/Newsletter/index.twig', $context);
    }
}
