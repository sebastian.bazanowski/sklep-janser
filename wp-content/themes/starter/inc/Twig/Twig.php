<?php

namespace Kreacja\Twig;

use Kreacja\Twig\Filters;
use Kreacja\Twig\Functions;
use Timber;

class Twig
{

    private $filters = [];

    private $functions = [];

    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        add_filter('timber/twig', [$this, 'registerFilters']);
        add_filter('timber/twig', [$this, 'registerFunctions']);

        $this->loadClasses();
    }

    /**
     * This function register new Twig Filters.
     *
     * @param  mixed $twig
     * @return void
     */

    public function registerFilters($twig)
    {
        foreach ($this->filters as $filter) {
            $namespaced_class = __NAMESPACE__ . '\\Filters\\' . $filter;
            $namespaced_class = new $namespaced_class();

            $twig->addFilter(new Timber\Twig_Filter(strtolower($filter), [$namespaced_class, 'init']));
        }

        return $twig;
    }

    /**
     * This function register new Twig Functions
     *
     * @param  mixed $twig
     * @return void
     */
    public function registerFunctions($twig)
    {
        foreach ($this->functions as $function) {
            $namespaced_class = __NAMESPACE__ . '\\Functions\\' . $function;
            $namespaced_class = new $namespaced_class();

            $twig->addFunction(new Timber\Twig_Function(strtolower($function), [$namespaced_class, 'init']));
        }

        return $twig;
    }

    /**
     * This function loads Twig Filters/Functions from Folder.
     *
     * @return void
     */
    public function loadClasses()
    {
        $filters = glob(get_template_directory() . '/inc/Twig/Filters/*.php');
        $functions = glob(get_template_directory() . '/inc/Twig/Functions/*.php');

        foreach ($filters as $filter) {
            $class = basename($filter, '.php');
            $this->filters[] = $class;
        }

        foreach ($functions as $function) {
            $class = basename($function, '.php');
            $this->functions[] = $class;
        }
    }

    /**
     * Function which add values to Timber context
     *
     * @param  string $key
     * @param  mixed $value
     * @return void
     */

    public function addToContext($key, $value)
    {
        add_action('init', function () use ($key, $value) {

            add_filter("timber/context", function ($context) use ($key, $value) {

                $context[$key] = $value;

                return $context;
            });
        });

    }
}
