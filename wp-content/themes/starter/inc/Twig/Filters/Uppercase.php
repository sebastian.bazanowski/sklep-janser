<?php

namespace Kreacja\Twig\Filters;

class Uppercase
{

    public function init($title)
    {
        return mb_strtoupper(html_entity_decode($title), 'UTF-8');
    }
}
