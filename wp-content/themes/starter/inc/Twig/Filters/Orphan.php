<?php

namespace Kreacja\Twig\Filters;

class Orphan
{

    public function init($content)
    {
        if (!class_exists('iWorks_Orphan')) {
            return $content;
        }

        $orphan = new iWorks_Orphan();

        return $orphan->replace($content);
    }
}
