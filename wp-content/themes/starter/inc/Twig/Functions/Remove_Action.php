<?php

namespace Kreacja\Twig\Functions;

class Remove_Action
{
    public function init($actionName, $hook, $priority, $params = 1)
    {
        remove_action($actionName, $hook, $priority, $params);
    }
}
