<?php

namespace Kreacja\Twig\Functions;

class SetupPostData
{

    public function init($post)
    {
        setup_postdata($GLOBALS['post'] = $post);
    }
}
