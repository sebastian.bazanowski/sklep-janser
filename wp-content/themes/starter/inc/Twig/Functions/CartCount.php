<?php

namespace Kreacja\Twig\Functions;

class CartCount
{
    public function init()
    {
        global $woocommerce;
        return $woocommerce->cart->cart_contents_count;
    }
}
