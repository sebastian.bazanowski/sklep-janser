<?php

namespace Kreacja\Twig\Functions;

class Term_Thumbnail
{

    public function init( $term_id )
    {
        $thumbnail_id = get_term_meta( $term_id, 'thumbnail_id', true );

        return wp_get_attachment_url( $thumbnail_id );
    }
}
