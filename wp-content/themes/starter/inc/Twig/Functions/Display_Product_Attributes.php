<?php

namespace Kreacja\Twig\Functions;

class Display_Product_Attributes
{
    public function init($cart_item_key)
    {
        $cart = WC()->cart->get_cart();
        $cart_item = $cart[$cart_item_key];

        $_product  = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

        $attributes = '';

        $attributes .= $_product->is_type('variable') || $_product->is_type('variation')  ? wc_get_formatted_variation($_product) : '';

        $attributes .= WC()->cart->get_item_data( $cart_item );

        echo $attributes;

    }
}
