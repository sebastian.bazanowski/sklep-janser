<?php

namespace Kreacja\Twig\Functions;

use Timber\Timber;

class Format_Image
{
    public function init( $image, $classes )
    {
        return Timber::compile("partials/picture.twig", [
            'image'     =>  $image,
            'classes'   =>  $classes,
        ]);
    }
}
