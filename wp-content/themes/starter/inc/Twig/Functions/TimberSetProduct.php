<?php

namespace Kreacja\Twig\Functions;

use Timber;

class TimberSetProduct
{

    public function init($post)
    {
        global $product;

        $product_id = null;

        if (is_int($post)) {
            $product_id = $post;
        } else {
            $product_id = $post->ID;
        }

        $context['post'] = Timber\Timber::get_post($product_id);

        $product = wc_get_product($product_id);

        return $product;
    }
}
