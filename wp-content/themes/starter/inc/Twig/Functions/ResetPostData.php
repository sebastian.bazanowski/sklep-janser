<?php

namespace Kreacja\Twig\Functions;

class ResetPostData
{

    public function init()
    {
        wp_reset_postdata();
    }
}
