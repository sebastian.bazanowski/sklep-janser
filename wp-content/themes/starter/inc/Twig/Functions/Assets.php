<?php

namespace Kreacja\Twig\Functions;

class Assets
{
    public function init($file = "")
    {
        return get_template_directory_uri() . $file;
    }
}
