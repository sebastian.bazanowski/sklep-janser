<?php

namespace Kreacja\Twig\Functions;

class Term_Has_Children
{

    public function init($term, $taxonomy)
    {
        $children = get_term_children($term, $taxonomy);

        if (!empty($children))
            return true;

        return false;
    }
}
