<?php

namespace Kreacja\Twig\Functions;

class VDump
{
    public function init($content)
    {
        echo "<pre style='background:cyan;padding:10px;z-index:1000000;color:#000;font-size:13px;'>";
        print_r($content);
        echo "</pre>";
    }
}
