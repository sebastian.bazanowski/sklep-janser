<?php

namespace Kreacja\Twig\Functions;

class Dump_Filters
{

    public function init($hook = "")
    {
        global $wp_filter;

        if (empty($hook) || !isset($wp_filter[$hook]))
            return;

        print '<pre>';
        print_r($wp_filter[$hook]);
        print '</pre>';
    }
}
