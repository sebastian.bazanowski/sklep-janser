<?php

namespace Kreacja\Twig\Functions;

use Timber\Timber;

class Icon
{
    public function init($fileName = "")
    {
        return Timber::compile("/icons/{$fileName}.twig");
    }
}
