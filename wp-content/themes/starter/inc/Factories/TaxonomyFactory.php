<?php

namespace Kreacja\Factories;

use Kreacja\Factories\Factory;

class TaxonomyFactory extends Factory
{
    public static function create($class)
    {
        add_action('init', [$class, 'init']);
    }
}
