<?php

namespace Kreacja\Factories;

use Kreacja\Factories\Factory;

class AjaxFactory extends Factory
{
    public static function create($class, $action_name = "")
    {
        add_action("wp_ajax_{$action_name}", [$class, 'init']);
        add_action("wp_ajax_nopriv_{$action_name}", [$class, 'init']);
    }
}
