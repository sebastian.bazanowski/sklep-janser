<?php

namespace Kreacja\Factories;

abstract class Factory
{
    abstract public static function create($className);
}