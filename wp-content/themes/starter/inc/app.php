<?php

use Kreacja\Acf\OptionSubpage;
use Kreacja\Blocks\Button\Button;
use Kreacja\Blocks\Newsletter\Newsletter;
use Kreacja\Blocks\News\News;
use Kreacja\Blocks\Knowledge\Knowledge;
use Kreacja\Blocks\Offer\Offer;
use Kreacja\Blocks\Reviews\Reviews;
use Kreacja\Blocks\Wysiwyg\Wysiwyg;
use Kreacja\Blocks\Arguments\Arguments;
use Kreacja\Blocks\Workers\Workers;
use Kreacja\Core\Hooks\Hooks;
use Kreacja\Singleton\App;
use Kreacja\Factories\AjaxFactory;
use Kreacja\Factories\PostTypeFactory;
use Kreacja\Modules\Search\Search;
use Kreacja\Helpers\WoocommerceHelper;


$app = App::getInstance();

$app->acf->addOptionSubpage(
    new OptionSubpage('Ustawienia motywu', 'Ustawienia motywu', 'themes.php')
);


add_filter("timber/context", function ($context) {

    $context['menu'] = new Timber\Menu('primary');
    $context['footer_menu_shopping'] = new Timber\Menu('footer_menu_shopping');
    $context['footer_menu_service'] = new Timber\Menu('footer_menu_service');
    $context['footer_menu_ship'] = new Timber\Menu('footer_menu_ship');
    $context['options'] = get_fields('options');
    $context['is_woocommerce_enabled'] = WoocommerceHelper::isWoocommerceActive();

    return $context;
});


$app->acf->addBlock(Button::class);
$app->acf->addBlock(Newsletter::class);
$app->acf->addBlock(News::class);
$app->acf->addBlock(Knowledge::class);
$app->acf->addBlock(Offer::class);
$app->acf->addBlock(Reviews::class);
$app->acf->addBlock(Arguments::class);
$app->acf->addBlock(Workers::class);
$app->acf->addBlock(Wysiwyg::class);


// TaxonomyFactory::create(HighlightedCategories::class);

// PostTypeFactory::create(Offer::class);

// new Search();
