<?php

namespace Kreacja\Helpers;

class WoocommerceHelper
{

    public static function isWoocommerceActive()
    {
        return filter_var($_ENV['IS_WOOCOMMERCE_ENABLED'], FILTER_VALIDATE_BOOLEAN);
    }
}
