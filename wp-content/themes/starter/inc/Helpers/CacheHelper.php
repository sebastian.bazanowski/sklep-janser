<?php

namespace Kreacja\Helpers;

class CacheHelper
{

    public static function isActive()
    {
        return filter_var($_ENV['IS_TIMBER_CACHE_ENABLED'], FILTER_VALIDATE_BOOLEAN);
    }

    public static function getCacheMethod()
    {
        switch ($_ENV['IS_TIMBER_CACHE_ENABLED_METHOD']) {
            case "CACHE_NONE":
                return \Timber\Loader::CACHE_NONE;
                break;
            case "CACHE_TRANSIENT":
                return \Timber\Loader::CACHE_TRANSIENT;
                break;
        }
    }
}
