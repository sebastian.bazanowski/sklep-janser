<?php

namespace Kreacja\Interfaces;

interface BlocksInterface
{
    public static function init();

    public static function renderCallback($block, $content = '', $is_preview = false);
}
