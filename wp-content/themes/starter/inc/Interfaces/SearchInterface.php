<?php

namespace Kreacja\Interfaces;

interface SearchInterface
{
    public function getData($data);
}
