<?php

namespace Kreacja\Core\PostTypes;

use Kreacja\Interfaces\PostTypesInterface;

class Offer implements PostTypesInterface
{
    public static function init()
    {
        $labels = array(
            'name'                => __('Oferta', 'starter'),
            'singular_name'       => __('Oferta', 'starter'),
            'menu_name'           => __('Oferta', 'starter'),
            'name_admin_bar'      => __('Oferta', 'starter'),
            'parent_item_colon'   => __('Parent Item:', 'starter'),
            'all_items'           => __('Wszystkie oferty', 'starter'),
            'add_new_item'        => __('Dodaj ofertę', 'starter'),
            'add_new'             => __('Dodaj nową ofertę', 'starter'),
            'new_item'            => __('Oferta', 'starter'),
            'edit_item'           => __('Edytuj ofertę', 'starter'),
            'update_item'         => __('Zaktualizuj ofertę', 'starter'),
            'view_item'           => __('Zobacz ofertę', 'starter'),
            'search_items'        => __('Szukaj ofertę', 'starter'),
            'not_found'           => __('Brak ofert', 'starter'),
            'not_found_in_trash'  => __('Brak ofert w koszu', 'starter'),
        );

        $args = array(
            'description'         => '',
            'labels'              => $labels,
            'supports'            => array('title', 'editor', 'thumbnail', 'revisions'),
            'show_in_rest'        => true,
            'taxonomies'          => array(),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'menu_position'       => 6,
            'show_in_admin_bar'   => true,
            'show_in_nav_menus'   => true,
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'rewrite'             => array('slug' => 'oferta'),
            'query_var'           => true,
            'capability_type'     => 'page',
            'menu_icon'            => 'dashicons-screenoptions',
        );

        register_post_type('offer', $args);
    }
}
