<?php

namespace Kreacja\Core\Hooks\Filters;

use Kreacja\Utils\KeyFromStringUtil;

class WoocommerceFilters
{

    public function __construct()
    {
        add_filter('loop_shop_columns', [$this, 'changeColumnsNumber'], 999);
        add_filter('woocommerce_cross_sells_columns', [$this, 'changeColumnsNumber']);
        add_filter('pre_get_posts', [$this, 'searchOnlyProducts']);
        add_filter('woocommerce_product_tabs', [$this, 'woocommerceShippingPriceTab']);
        add_filter('woocommerce_add_to_cart_fragments', [$this, 'updateCartIconInTheHeader'], 9999, 1);
        add_filter('woocommerce_checkout_fields', [$this, 'modifyCheckoutFormLabels'], 9999);
        add_filter('woocommerce_product_tabs', [$this, 'woocommerceProductTabs'], 999);
        add_filter('get_avatar_data', [$this, 'useProductImgAsAvatarInReviews'], 100, 2);
    }

    public function useProductImgAsAvatarInReviews($args, $user_data)
    {
        global $product;

        if ($product) {
            $args['url'] = wp_get_attachment_url($product->get_image_id());
        }

        return $args; 
    }

    public function woocommerceProductTabs($tabs)
    {
        $globalTabs = is_array(get_field('product_tabs', 'option')) ? get_field('product_tabs', 'option') : [];

        $productTabs = is_array(get_field('product_tabs')) ? get_field('product_tabs') : [];

        $tabsToAdd = array_merge($globalTabs, $productTabs);

        foreach ($tabsToAdd as $tab) {

            $exludeCategories = is_array($tab['exclude_from_category']) ? $tab['exclude_from_category'] : [];

            $isExcluded = in_array(1, array_map(function ($category) use ($exludeCategories) {
                return in_array($category->term_id, $exludeCategories);
            }, get_the_terms(get_queried_object_id(), 'product_cat')));


            if ($isExcluded) continue;

            $key = KeyFromStringUtil::make($tab['title']);

            $tabs[$key] = [
                'title'     =>  $tab['title'],
                'callback'  => function () use ($tab) {
                    echo $tab['content'];
                }
            ];
        };

        if (isset($tabs['additional_information'])) {
            $tabs['additional_information']['title'] = __('Parametry', 'starter');
        }

        return $tabs;
    }

    public function updateCartIconInTheHeader($fragments)
    {
        $cart_items_count = WC()->cart->cart_contents_count;
        ob_start(); ?>

        <?php if ($cart_items_count == 0) : ?>
            <span class="CartIndicator__dot absolute -top-1 -right-1 h-0 w-0 bg-primary rounded-full"></span>
        <?php else : ?>
            <span class="CartIndicator__dot absolute -top-1 -right-1 h-2 w-2 bg-primary rounded-full"></span>
        <?php endif; ?>

<?php $fragments['.CartIndicator .CartIndicator__dot'] = ob_get_clean();
        return $fragments;
    }

    public function changeColumnsNumber()
    {
        $colums = get_field('column_number', 'options');
        return $colums;
    }

    public function searchOnlyProducts($query)
    {
        if ($query->is_search && !$query->is_admin() && $query->is_main_query()) {
            $query->set('post_type', ['product']);
        }

        return $query;
    }

    public function woocommerceShippingPriceTab($tabs)
    {
        $tabs['shipping_price'] = [
            'title'     =>  __('Koszty wysyłki', 'starter'),
            'priority'  => 50,
            'callback'  => [$this, "woocommerce_add_shipping_price"]
        ];

        return $tabs;
    }

    public function woocommerce_add_shipping_price()
    {
        $product = wc_get_product();

        $zones = \WC_Shipping_Zones::get_zones();

        echo "<table class='woocommerce-product-attributes shop_attributes'>";
        echo "<tr class='woocommerce-product-attributes-item'>";
        echo "<th class='woocommerce-product-attributes-item__label'>";
        echo "Metoda wysyłki";
        echo "</th>";
        echo "<th class='woocommerce-product-attributes-item__label'>";
        echo "Liczba sztuk";
        echo "</th>";
        echo "<th>";
        echo "Kwota";
        echo "</th>";
        echo "</tr>";

        foreach ($zones as $zone) {
            foreach ($zone['shipping_methods'] as $shipping_method) {
                if (array_key_exists('method_rules', $shipping_method->instance_settings)) {
                    foreach ($shipping_method->instance_settings['method_rules']  as $rule) {
                        foreach ($rule['conditions'] as $cond) {
                            $methodTitle = isset($shipping_method->instance_settings['method_title']) ? $shipping_method->instance_settings['method_title'] : "---------";
                            $minAmount = $cond['min'];
                            $maxAmount = $cond['max'];
                            $minAmountHTML =  !empty($maxAmount) ? $minAmount : $minAmount . '+';
                            $maxAmountHTML = !empty($maxAmount) ? " - " . $maxAmount  : "";
                            $cost = wc_price($rule['cost_per_order']);
                            echo "
                                <tr class='woocommerce-product-attributes-item'> 
                                <td class='woocommerce-product-attributes-item__value'>{$methodTitle} </td> 
                                <td class='woocommerce-product-attributes-item__value'>{$minAmountHTML} {$maxAmountHTML} </td> 
                                <td class='woocommerce-product-attributes-item__value'> {$cost} </td> 
                                </tr>";
                        }
                    }
                }
            }
        }
        // foreach ($zones as $zone) {
        //     foreach ($zone['shipping_methods'] as $shipping_method) {
        //         if (array_key_exists('method_rules', $shipping_method->instance_settings)) {
        //             foreach ($shipping_method->instance_settings['method_rules']  as $rule) {
        //                 foreach ($rule['conditions'] as $condition) {
        //                     if (array_key_exists('shipping_class', $condition)) {
        //                         if (in_array($product->get_shipping_class_id(), $condition['shipping_class'])) {
        //                             foreach ($rule['conditions'] as $cond) {
        //                                 if ($cond['condition_id'] == "item") {
        //                                     $methodTitle = isset($shipping_method->instance_settings['method_title']) ? $shipping_method->instance_settings['method_title'] : "---------";
        //                                     $minAmount = $cond['min'];
        //                                     $maxAmount = $cond['max'];
        //                                     $minAmountHTML =  !empty($maxAmount) ? $minAmount : $minAmount . '+';
        //                                     $maxAmountHTML = !empty($maxAmount) ? " - " . $maxAmount  : "";
        //                                     $cost = wc_price($rule['cost_per_order']);
        //                                     echo "
        //                                         <tr class='woocommerce-product-attributes-item'> 
        //                                         <td class='woocommerce-product-attributes-item__value'>{$methodTitle} </td> 
        //                                         <td class='woocommerce-product-attributes-item__value'>{$minAmountHTML} {$maxAmountHTML} </td> 
        //                                         <td class='woocommerce-product-attributes-item__value'> {$cost} </td> 
        //                                         </tr>";
        //                                 }
        //                             }
        //                         }
        //                     }
        //                 }
        //             }
        //         }
        //     }
        // }

        echo "</table>";
    }

    public function modifyCheckoutFormLabels($fields)
    {
        foreach ($fields as &$field_type) {
            foreach ($field_type as &$field) {
                $field['placeholder'] = $field['label'];
                $field['label'] = "";
            }
        };

        return $fields;
    }
}
