<?php

namespace Kreacja\Core\Hooks;

use Kreacja\Core\Hooks\Actions\WordpressActions;
use Kreacja\Core\Hooks\Actions\WoocommerceActions;
use Kreacja\Core\Hooks\Filters\WordpressFilters;
use Kreacja\Core\Hooks\Filters\WoocommerceFilters;
use Kreacja\Helpers\WoocommerceHelper;

final class Hooks
{
    public function __construct()
    {
        new WordpressActions();
        new WordpressFilters();

        if (WoocommerceHelper::isWoocommerceActive()) {
            new WoocommerceActions();
            new WoocommerceFilters();
        }
    }
}
