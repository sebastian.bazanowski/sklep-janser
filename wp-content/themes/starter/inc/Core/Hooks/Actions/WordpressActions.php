<?php

namespace Kreacja\Core\Hooks\Actions;

class WordpressActions
{

    public function __construct()
    {
        add_action('admin_enqueue_scripts', [$this, 'my_admin_style']);
    }

    public function my_admin_style()
    {
        wp_enqueue_style('admin-style', get_template_directory_uri() . '/assets/css/admin-style.css');
    }
}
