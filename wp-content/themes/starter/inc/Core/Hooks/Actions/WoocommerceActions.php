<?php

namespace Kreacja\Core\Hooks\Actions;

use Timber\Timber;

class WoocommerceActions
{
    public function __construct()
    {
        add_action('woocommerce_single_product_summary', [$this, 'display_products_relations'], 25);
        add_action('woocommerce_checkout_process', [$this, 'sk_custom_checkout_field_process']);
        add_action('woocommerce_add_to_cart',array($this,'set_last_added_cart_item_key'),10,6);
    }

    public function set_last_added_cart_item_key($cart_item_key, $product_id, $quantity, $variation_id, $variation, $cart_item_data)
    {
        update_option('kreacja_last_added_to_cart_key', $cart_item_key);
    }

    public function sk_custom_checkout_field_process()
    {
        if (!$_POST['privacy']) {
            wc_add_notice(__('Należy zapoznać się z polityką prywatności.'), 'error');
        }
        if (!$_POST['terms']) {
            wc_add_notice(__('Należy zaakceptować regulamin.'), 'error');
        }
    }

    public function display_products_relations()
    {
        $current_product_id = get_queried_object_id();

        $relation_fields = get_field('products_relations_configurator', 'options');

        $relation_products = [];
        foreach ($relation_fields as $key => $field) {
            foreach ($field['related_products'] as $product) {
                if ($current_product_id === $product->ID) {
                    $relation_products = array_map(function ($single) {
                        return $single->ID;
                    }, $relation_fields[$key]['related_products']);
                }
            }
        }

        echo Timber::compile('woocommerce/single-product/elements/relations.twig', [
            'relation_products' =>  $relation_products
        ]);
    }
}
