<?php

namespace Kreacja\Core;

use Kreacja\Helpers\WoocommerceHelper;
use Kreacja\Core\Hooks\Hooks;

class Theme
{
    function __construct()
    {
        add_action('after_setup_theme', [$this, 'themeSetup']);
        add_action('widgets_init', [$this, 'themeWidgetsInit']);
        add_action('wp_enqueue_scripts', [$this, 'themeScripts'], 100);
        // add_filter('clean_url', [$this, 'optimizeJS'], 11, 1);

        new Hooks();
    }

    public function themeSetup()
    {
        $font_url = str_replace(',', '%2C', '//fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap');
        add_editor_style($font_url);

        add_theme_support('automatic-feed-links');
        add_theme_support('title-tag');
        add_theme_support('post-thumbnails');
        add_theme_support('html5', ['search-form', 'comment-form', 'comment-list', 'gallery', 'caption']);

        if (WoocommerceHelper::isWoocommerceActive()) {
            add_theme_support('woocommerce');
        }

        register_nav_menus(
            [
                'primary' => __('Menu główne', 'starter'),
                'footer_menu_shopping' => __('Menu w stopce Zakupy', 'starter'),
                'footer_menu_service' => __('Menu w stopce  Serwis i gwarancja', 'starter'),
                'footer_menu_ship' => __('Menu w stopce Warunki dostawy ', 'starter')
            ]
        );
    }

    public function themeWidgetsInit()
    {
        // Place to register sidebars
    }

    public function themeScripts()
    {
        // wp_enqueue_style('starter', get_stylesheet_uri());

        $ver = $_ENV['MODE'] === 'development' ? time() : '1.0.0';

        wp_enqueue_script('bundle-js', get_template_directory_uri() . '/dist/js/main.js', array('jquery'), $ver, true);

        wp_localize_script('bundle-js', 'jsData', ['ajaxUrl' => admin_url('admin-ajax.php'), 'siteUrl' => site_url()]);

        if (is_singular() && comments_open() && get_option('thread_comments'))
            wp_enqueue_script('comment-reply');

        if (WoocommerceHelper::isWoocommerceActive()) {
            wp_dequeue_style('woocommerce-general');
            wp_dequeue_style('woocommerce-layout');
            wp_dequeue_style('woocommerce-smallscreen');

            if (is_woocommerce()) {
                wp_dequeue_style('wp-block-library');
                wp_dequeue_style('wp-block-library-theme');
                wp_dequeue_style('wc-blocks-style');
            }

            if (is_product()) {
                wp_dequeue_style('font-awesome');
                wp_dequeue_style('berocket_aapf_widget-style');
            }
        }
    }

    public function optimizeJS($url)
    {
        if (is_admin()) {
            return $url;
        }
        if (FALSE === strpos($url, '.js'))
            return $url;

        // if (strpos($url, 'jquery'))
        //     return $url;

        return "$url' defer ";
        return $url;
    }
}
