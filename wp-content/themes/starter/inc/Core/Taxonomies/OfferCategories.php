<?php

namespace Kreacja\Core\Taxonomies;

use Kreacja\Interfaces\TaxonomiesInterface;

class OfferCategories implements TaxonomiesInterface
{
    public static function init()
    {
        $labels = array(
            'name'                       => _x('Kategorie oferty', 'Taxonomy General Name', 'starter'),
            'singular_name'              => _x('Kategoria oferty', 'Taxonomy Singular Name', 'starter'),
            'menu_name'                  => __('Kategorie oferty', 'starter'),
            'all_items'                  => __('Wszystkie kategorie', 'starter'),
            'parent_item'                => __('Parent Item', 'starter'),
            'parent_item_colon'          => __('Parent Item:', 'starter'),
            'new_item_name'              => __('kategoria', 'starter'),
            'add_new_item'               => __('Dodaj kategorię', 'starter'),
            'edit_item'                  => __('Edytuj', 'starter'),
            'update_item'                => __('Zaktualizuj', 'starter'),
            'view_item'                  => __('Zobacz kategorię', 'starter'),
            'separate_items_with_commas' => __('Separate items with commas', 'starter'),
            'add_or_remove_items'        => __('Dodaj lub usuń kategorię', 'starter'),
            'choose_from_most_used'      => __('Wybierz najczęściej używaną', 'starter'),
            'popular_items'              => __('Popularne kategorie', 'starter'),
            'search_items'               => __('Szukaj', 'starter'),
            'not_found'                  => __('Brak kategorii', 'starter'),
        );

        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'rewrite'                      => array('hierarchical' => true),
            'show_tagcloud'              => true,
        );

        register_taxonomy('offer_categories', array('offer'), $args);
    }
}
