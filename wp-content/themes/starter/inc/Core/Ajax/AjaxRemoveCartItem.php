<?php

namespace Kreacja\Core\Ajax;

use Kreacja\Interfaces\AjaxInterface;

class AjaxRemoveCartItem implements AjaxInterface
{
     public static function init()
     {
          $data = $_GET['productKey'];

          $query_string = parse_url($data, PHP_URL_QUERY);

          parse_str($query_string, $params);

          foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
               if ($cart_item_key === $params['remove_item']) {
                    WC()->cart->remove_cart_item($cart_item_key);
               }
          }

          wp_send_json(['status' => 'success']);

          wp_die();
     }
}
