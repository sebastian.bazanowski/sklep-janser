<?php

namespace Kreacja\Core\Ajax;

use Kreacja\Interfaces\AjaxInterface;

class AjaxGetLatestProductATC implements AjaxInterface
{
    public static function init()
    {
        $cart_item_key = get_option('kreacja_last_added_to_cart_key');

        $cart = WC()->cart->get_cart();

        $cart_item = $cart[$cart_item_key];

        $_product   = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);

        $product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);

        $product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item, $cart_item_key);

        $thumbnail         = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key);

        $product_name     =  apply_filters('woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key) . '&nbsp;';

        $product_price     = apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key);

        $product_subtotal = apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key);

        // Meta data
        $attributes = '';

        //Variation
        $attributes .= $_product->is_type('variable') || $_product->is_type('variation')  ? wc_get_formatted_variation($_product) : '';
        // Meta data
        if (version_compare(WC()->version, '3.3.0', "<")) {
            $attributes .=  WC()->cart->get_item_data($cart_item);
        } else {
            $attributes .=  wc_get_formatted_cart_item_data($cart_item);
        }

        ob_start();

?>

        <div>
            <a class="popup-image" href="<?php echo $product_permalink; ?>">
                <?php echo $thumbnail; ?>
            </a>
            <h3 class="mt-2 text-lg font-normal text-primary"><?php echo $product_name; ?></h3>
            <div class="text-sm text-primary/70 my-2"><?php echo $attributes; ?></div>
            <p class="font-bold text-lg text-primary mb-4"><?php echo $product_price; ?></p>
        </div>

<?php

        wp_send_json_success([
            'content'   =>  ob_get_clean(),
        ]);

        wp_die();
    }
}
