<?php

namespace Kreacja\Core\Ajax;

use Kreacja\Twig\Functions\Display_Product_Attributes;
use Kreacja\Interfaces\AjaxInterface;

class AjaxGetCart implements AjaxInterface
{
    public static function init()
    {

        $output = [
            'cart_count' => WC()->cart->get_cart_contents_count(),
            'cart_total' => wc_price(WC()->cart->get_subtotal()),
            'cart_items' => self::renderItemsData(),
        ];

        wp_send_json($output);

        die();
    }

    public static function renderItemsData()
    {
        ob_start();
?>
        <?php if (!WC()->cart->is_empty()) : ?>
            <?php foreach (WC()->cart->get_cart() as $key => $item) : ?>
                <div class="CartItem">
                    <a class="CartItem__image" href="<?php echo get_permalink($item['product_id']); ?>">
                        <img width="300" height="200" src="<?php echo get_the_post_thumbnail_url($item['product_id'], 'woocommerce_gallery_thumbnail'); ?>" alt="<?php echo get_the_title($item['product_id']); ?>">
                    </a>
                    <div class="CartItem__wrapper">
                        <h2 class="CartItem__title">
                            <?php echo get_the_title($item['product_id']); ?>
                        </h2>
                        <div class="CartItem__meta">
                            <div class="CartItem__attributes">
                                <?php (new Display_Product_Attributes)->init($key); ?>
                            </div>
                            <p class="CartItem__pricePerSingle">
                                <?php echo WC()->cart->get_product_price($item['data']); ?> / szt.
                            </p>
                            <p class="CartItem__quantity"><?= __('Ilość', 'starter'); ?>: <?php echo $item['quantity']; ?></p>
                        </div>
                    </div>
                    <div class="product-remove CartItem__remove">
                        <button data-key="<?php echo esc_url(wc_get_cart_remove_url($key)); ?>" class="Button--no-style remove">
                            ×
                        </button>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php else : ?>
            <p class="Cart__noProductsInfo">
                <?php _e('Brak produktów w koszyku', 'polcase'); ?>
            </p>
        <?php endif; ?>

<?php return ob_get_clean();
    }
}
