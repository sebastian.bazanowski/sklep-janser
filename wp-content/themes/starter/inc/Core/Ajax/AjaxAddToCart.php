<?php

namespace Kreacja\Core\Ajax;

use Kreacja\Interfaces\AjaxInterface;

class AjaxAddToCart implements AjaxInterface
{
    public static function init()
    {

        $errors = wc_get_notices('error');

        if ($errors) {
            ob_start();
            foreach ($errors as $error) {
                wc_print_notice($error, 'error');
            }

            $js_data =  [
                'error' => ob_get_clean()
            ];

            wc_clear_notices();
            wp_send_json($js_data);
        } else {

            do_action('woocommerce_ajax_added_to_cart', intval($_POST['add-to-cart']));

            wc_clear_notices();

            \WC_AJAX::get_refreshed_fragments();
        }

        die();
    }
}
