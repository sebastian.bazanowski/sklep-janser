<?php

namespace Kreacja\Core\Ajax;

use Kreacja\Interfaces\AjaxInterface;

class AjaxSearch implements AjaxInterface
{
    public static function init()
    {
        $args = [
            'post_type' => 'product',
            'posts_per_page' => -1,
            's' => $_POST['s']
        ];
        $q = new WP_Query($args);
        $posts = [];

        if ($q->have_posts()) {
            while ($q->have_posts()) : $q->the_post();
                $posts[] = [
                    "id" => get_the_ID(),
                    "name" => get_the_title(),
                    "link" => get_the_permalink()
                ];
            endwhile;

            wp_reset_postdata();
            wp_send_json_success($posts);
        } else {
            wp_send_json_error("Something went wrong!");
        }
    }
}
