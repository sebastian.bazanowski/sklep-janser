<?php

/**
 * Checkout terms and conditions area.
 *
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined('ABSPATH') || exit;

if (apply_filters('woocommerce_checkout_show_terms', true) && function_exists('wc_terms_and_conditions_checkbox_enabled')) {
	do_action('woocommerce_checkout_before_terms_and_conditions');

?>
	<div class="woocommerce-terms-and-conditions-wrapper">
		<?php
		/**
		 * Terms and conditions hook used to inject content.
		 *
		 * @since 3.4.0.
		 * @hooked wc_checkout_privacy_policy_text() Shows custom privacy policy text. Priority 20.
		 * @hooked wc_terms_and_conditions_page_content() Shows t&c page content. Priority 30.
		 */
		do_action('woocommerce_checkout_terms_and_conditions');
		?>

		<div class="woocommerce-terms-and-conditions-wrapper flex flex-col my-8">
			<label class="mb-4" for="privacy">
				<input class="mr-2" type="checkbox" name="privacy" id="privacy">
				<span class="text-sm"><?= __('Zapoznałem/am się z <a href="' . home_url() . '/warunki-zakupow/polityka-prywatnosci-i-cookies/" target="_blank">polityką prywatności</a>.', 'starter'); ?><span class="is-required">*</span></span>
			</label>
			<label class="mb-4" for="terms">
				<input class="mr-2" type="checkbox" name="terms" id="terms">
				<span class="text-sm"><?= __('Przeczytałem/am i akceptuję <a href="' . home_url() . '/warunki-zakupow/regulamin/" target="_blank">regulamin</a>.', 'starter'); ?><span class="is-required">*</span></span>
			</label>
			<label class="mb-4" for="marketing">
				<input class="mr-2" type="checkbox" name="marketing" id="marketing">
				<span class="text-sm"><?= __('Wyrażam zgodę na otrzymywanie wiadomości marketingowych i informacji o ofertach na mój e-mail.', 'starter'); ?></span>
			</label>
		</div>
	</div>
<?php

	do_action('woocommerce_checkout_after_terms_and_conditions');
}
