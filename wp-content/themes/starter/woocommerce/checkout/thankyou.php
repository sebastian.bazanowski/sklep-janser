<?php

/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.7.0
 */

defined('ABSPATH') || exit;
?>

<div class="CartCheckoutBreadcrumbs">
	<p class="CartCheckoutBreadcrumbs__item"><?= __('Koszyk', 'starter'); ?></p>
	<span class="CartCheckoutBreadcrumbs__separator"></span>
	<p class="CartCheckoutBreadcrumbs__item"><?= __('Zamówienie', 'starter'); ?></p>
	<span class="CartCheckoutBreadcrumbs__separator"></span>
	<p class="CartCheckoutBreadcrumbs__item CartCheckoutBreadcrumbs__item-active"><?= __('Potwierdzenie', 'starter'); ?></p>
</div>

<div class="ThankYouPage woocommerce-order">

	<?php
	if ($order) :

		do_action('woocommerce_before_thankyou', $order->get_id());
	?>

		<?php if ($order->has_status('failed')) : ?>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php esc_html_e('Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce'); ?></p>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
				<a href="<?php echo esc_url($order->get_checkout_payment_url()); ?>" class="button pay"><?php esc_html_e('Pay', 'woocommerce'); ?></a>
				<?php if (is_user_logged_in()) : ?>
					<a href="<?php echo esc_url(wc_get_page_permalink('myaccount')); ?>" class="button pay"><?php esc_html_e('My account', 'woocommerce'); ?></a>
				<?php endif; ?>
			</p>

		<?php else : ?>

			<p class="ThankYouPage__success-info woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters('woocommerce_thankyou_order_received_text', esc_html__('Thank you. Your order has been received.', 'woocommerce'), $order); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped 
																																		?></p>

			<?php if ($order->get_payment_method() == "bacs") : ?>
				<div class="ThankYouPage__order-details">
					<div>
						<p><?= __('Konto bankowe', 'sleephoem'); ?></p>
						<?php
						$bacs_accounts_info = get_option('woocommerce_bacs_accounts');
						?>
						<strong><?php echo $bacs_accounts_info[0]['account_number']; ?></strong>
					</div>
					<div>
						<p><?= __('Dane odbiorcy', 'starter'); ?></p>
						<strong><?php echo get_field('recepient-name', 'options'); ?></strong>
						<strong><?php echo get_field('recepient-address', 'options'); ?></strong>
					</div>
					<div>
						<p><?= __('Tytuł przelewu', 'starter'); ?></p>
						<strong class="order-title"><?php echo $order->get_id(); ?></strong>
					</div>
					<div>
						<p><?= __('Do zapłaty', 'starter'); ?></p>
						<strong class="order-cost"><?php echo $order->get_formatted_order_total(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped 
													?></strong>
					</div>
				</div>
			<?php endif; ?>

		<?php endif; ?>

		<?php do_action('woocommerce_thankyou', $order->get_id()); ?>

	<?php else : ?>

		<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters('woocommerce_thankyou_order_received_text', esc_html__('Thank you. Your order has been received.', 'woocommerce'), null); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped 
																										?></p>

	<?php endif; ?>

</div>