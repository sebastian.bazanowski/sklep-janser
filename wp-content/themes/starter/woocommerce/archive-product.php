<?php

use Timber\Timber;

$context = Timber::context();
$context['posts'] = Timber::get_posts();
$context['columns'] = get_field('column_number', 'options');

$terms_args = [
    'taxonomy'      =>  'product_cat',
    'hide_empty'    =>  false,
    'exclude'       =>  [17]
];

$qo = get_queried_object();
if ($qo instanceof WP_Term) {
    $terms_args['parent'] = $qo->term_id;
    $context['display_type'] = get_term_meta($qo->term_id)['display_type'][0];
}

$context['categories'] = Timber::get_terms($terms_args);

Timber::render('woocommerce/archive-product/archive-product.twig', $context,  $_ENV['IS_TIMBER_CACHE_ENABLED_PERIOD'], Kreacja\Helpers\CacheHelper::getCacheMethod());
