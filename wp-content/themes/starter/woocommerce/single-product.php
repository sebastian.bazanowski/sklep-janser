<?php

use Timber\Timber;

$context = Timber::context();
$context['post'] = Timber::get_post();
$product = wc_get_product($context['post']->ID);
$context['product'] = $product;
$context['fields'] = get_fields();

$context['upsell_products'] = Timber::get_posts($product->get_upsell_ids());
wp_reset_postdata();

$context['cross_sell_products'] = Timber::get_posts($product->get_cross_sell_ids());
wp_reset_postdata();

$related_limit               = wc_get_loop_prop('columns');
$related_ids                 = wc_get_related_products($context['post']->id, $related_limit);
$context['related_products'] = Timber::get_posts($related_ids);

$product_images =  $context['product']->get_gallery_image_ids();
$product_image = get_post_thumbnail_id($context['product']->get_id());
$context['product_images'] = array_merge([$product_image], $product_images);
wp_reset_postdata();

Timber::render('woocommerce/single-product/single-product.twig', $context,  $_ENV['IS_TIMBER_CACHE_ENABLED_PERIOD'], Kreacja\Helpers\CacheHelper::getCacheMethod());
