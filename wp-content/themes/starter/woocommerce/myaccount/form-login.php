<?php

/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 4.1.0
 */

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly.
}
?>

<div class="MyAccountLogin">

	<?php do_action('woocommerce_before_customer_login_form'); ?>

	<?php if ('yes' === get_option('woocommerce_enable_myaccount_registration')) : ?>

		<div class="u-columns col2-set MyAccountLogin__columns" id="customer_login">

			<div class="u-column1 col-1 MyAccountLogin__column">

			<?php endif; ?>

			<h2 class="MyAccountLogin__title font-bold"><?php esc_html_e('Witaj z powrotem!', 'woocommerce'); ?></h2>
			<div class="MyAccountLogin__text">
				<p class="text-xl mt-2"><?php _e('Zaloguj się na swoje konto.', 'starter') ?></p>
			</div>

			<form class="woocommerce-form woocommerce-form-login login" method="post">

				<?php do_action('woocommerce_login_form_start'); ?>

				<div class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<input type="text" class="MyAccountLogin__input woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" placeholder="Adres e-mail" autocomplete="username" value="<?php echo (!empty($_POST['username'])) ? esc_attr(wp_unslash($_POST['username'])) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
					<label for="email">Adres e-mail</label>
				</div>
				<div class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<input class="MyAccountLogin__input woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" placeholder="Hasło" autocomplete="current-password" />
					<label for="password">Hasło</label>
				</div>

				<?php do_action('woocommerce_login_form'); ?>
				<div class="flex justify-between my-4">
				<p class="form-row mb-0">
					<label class="woocommerce-form__label woocommerce-form__label-for-checkbox woocommerce-form-login__rememberme">
						<input class="woocommerce-form__input woocommerce-form__input-checkbox mr-4" name="rememberme" type="checkbox" id="rememberme" value="forever" /> <span class="text-sm"><?php esc_html_e('Remember me', 'woocommerce'); ?></span>
					</label>
				</p>
				<p class="woocommerce-LostPassword lost_password flex items-center">
					<a class="text-primaryBlue text-sm" href="<?php echo esc_url(wp_lostpassword_url()); ?>"><?php esc_html_e('Lost your password?', 'woocommerce'); ?></a>
				</p>
	</div>
				<?php wp_nonce_field('woocommerce-login', 'woocommerce-login-nonce'); ?>
					<button type="submit" class="MyAccountLogin__button Button--secondary !bg-primaryBlue !text-white hover:!bg-secondaryGreen" name="login" value="<?php esc_attr_e('Log in', 'woocommerce'); ?>"><?php esc_html_e('Log in', 'woocommerce'); ?></button>

				<?php do_action('woocommerce_login_form_end'); ?>

			</form>

			<?php if ('yes' === get_option('woocommerce_enable_myaccount_registration')) : ?>

			</div>

			<div class="u-column2 col-2 MyAccountLogin__column">

				<h2 class="MyAccountLogin__title font-bold"><?php esc_html_e('Nie masz konta?', 'woocommerce'); ?></h2>
				<div class="MyAccountLogin__text">
					<p class="text-xl mt-2"><?php _e('Zarejestruj się i dołącz do nas.', 'starter'); ?></p>
				</div>

				<form method="post" class="woocommerce-form woocommerce-form-register register" <?php do_action('woocommerce_register_form_tag'); ?>>

					<?php do_action('woocommerce_register_form_start'); ?>

					<?php if ('no' === get_option('woocommerce_registration_generate_username')) : ?>

						<div class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
							<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" placeholder="<?php _e('Imię', 'starter'); ?>" id="reg_username" autocomplete="username" value="<?php echo (!empty($_POST['username'])) ? esc_attr(wp_unslash($_POST['username'])) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
							<label for="username">Imię</label>
					</div>

					<?php endif; ?>

					<div class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
						<input type="email" placeholder="<?php _e('Adres e-mail', 'starter') ?>" class="woocommerce-Input woocommerce-Input--text input-text" name="email" id="reg_email" autocomplete="email" value="<?php echo (!empty($_POST['email'])) ? esc_attr(wp_unslash($_POST['email'])) : ''; ?>" /><?php // @codingStandardsIgnoreLine 
																																																																												?>
							<label for="email">Adres e-mail</label>
					</div>

					<?php if ('no' === get_option('woocommerce_registration_generate_password')) : ?>

						<div class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
							<input type="password" placeholder="<?php _e('mojehaslo123', 'starter'); ?>" class="woocommerce-Input woocommerce-Input--text input-text" name="password" id="reg_password" autocomplete="new-password" />
							<label for="password">mojehaslo123</label>
					</div>

					<?php else : ?>

						<p><?php esc_html_e('A password will be sent to your email address.', 'woocommerce'); ?></p>

					<?php endif; ?>


					<div class="flex items-center mt-4">
					<input class="woocommerce-form__input woocommerce-form__input-checkbox mr-4" name="privacy" type="checkbox" id="privacy" required />
					<?php do_action('woocommerce_register_form'); ?>
					</div>


					<p class='my-4 text-sm'><?php _e('Pola oznaczone * są wymagane') ?></p>

					<p class="woocommerce-form-row form-row">
						<?php wp_nonce_field('woocommerce-register', 'woocommerce-register-nonce'); ?>
						<button type="submit" class="MyAccountLogin__button Button--secondary font-bold" name="register" value="<?php esc_attr_e('Register', 'woocommerce'); ?>"><?php esc_html_e('Register', 'woocommerce'); ?></button>
					</p>


					<?php do_action('woocommerce_register_form_end'); ?>

				</form>

			</div>

		</div>
	<?php endif; ?>

	<?php do_action('woocommerce_after_customer_login_form'); ?>
</div>
	
