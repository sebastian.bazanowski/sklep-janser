<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'janser' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'fk8@t;,ZUU~yk5`P:oWQ.j$~A}{i(I&DDuw4Z6bM%wXOW+Dr)>YC/VWdMeh&x0=5' );
define( 'SECURE_AUTH_KEY',  'A}HNZ+*Au^eQi:#tE5>i~%p$.>sKWrn-Fr@I_`EL.{s{fY*J{j!y0S==PC2LW#5y' );
define( 'LOGGED_IN_KEY',    '/nQm<md)UNZ[J8kCB;+8m|P^MKlSIsDZDe^>ug7~D|.&b%J4?~q=+FA1tJ1)cu?g' );
define( 'NONCE_KEY',        ';^ 37TutIDhR<8d7IL=57Na&Tv`vM/CEKx!i@r6=BLjlAV|P29NxDL%xf<9cG}8V' );
define( 'AUTH_SALT',        'U,mH mhNF4<Y+3Nz+^F2j2Jvk1Nd}P;c!6(hVN<?ts|PhUOEGqo)ha]t*)dE>j-F' );
define( 'SECURE_AUTH_SALT', 'tSv+48_PvV2e1VzP9oli6 a>&D!9YZ_w?#ei@C_Rv[}2`4ary8N~qTqY#iy$E[l8' );
define( 'LOGGED_IN_SALT',   'Qrs[<MiZRyA%(c[1Rn(#ive!dQ*D/3tQp]`hJXm3xs_p+d<Ba<M^i$#;,%4Ql&6r' );
define( 'NONCE_SALT',       'g^q2+qwgu{js75X|(LhsuR=CC<%*H J{aOpoUPPf,9*~6B*onZz7 ~xWc(}eqv!=' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'sk_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );
define( 'WP_POST_REVISIONS', 18 );
define( 'WP_AUTO_UPDATE_CORE', true );
define( 'DISALLOW_FILE_EDIT', true );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
